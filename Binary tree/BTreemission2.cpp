#include <iostream>
#include <string>
#include <queue>
#include <vector>

using namespace std;
class Btree {
    private:
        string val;
        Btree* left;
        Btree* right;
        queue<Btree*> myq;
        int* poped;
        // vector<bool> tree (4, 1);
    public:
        Btree* head;
        Btree() {
            poped = new int();
            *poped = 0;
        }
        void insert(string value) {
            Btree* temp = new Btree();
            temp->left = NULL;
            temp->right = NULL;
            if(value == "null") {
                myq.push(NULL);
                if((myq.size() + *poped) % 2 == 1) {
                    myq.pop();
                    (*poped) ++;
                }
            } else {
                temp->val = value;
                if(myq.empty()) {
                    myq.push(temp);
                    head = new Btree();
                    head = temp;
                }else{
                    myq.push(temp);
                    if( (myq.size() + *poped) % 2 == 0) {
                        if(myq.front() != NULL){
                            myq.front()->left = temp;
                        }
                    } else { 
                        if(myq.front() != NULL) {
                            myq.front()->right = temp;
                            myq.pop();
                            (*poped)++;
                        } else {
                            myq.pop();
                            (*poped)++;
                        }
                    }
                }
            }
        }
        void PreOrder(Btree* tmp){
            cout << tmp->val << " ";
            if(tmp->left != NULL)
                PreOrder(tmp->left);
            if(tmp->right != NULL)
                PreOrder(tmp->right);
        }
        void InOrder(Btree* tmp){
            if(tmp->left != NULL)    
                InOrder(tmp->left);
            cout << tmp->val << " ";
            if(tmp->right != NULL)
                InOrder(tmp->right);
        }
        void PostOrder(Btree* tmp){
            if(tmp->left != NULL)    
                PostOrder(tmp->left);
            if(tmp->right != NULL)
                PostOrder(tmp->right);
            cout << tmp->val << " ";
        }
}; 


int main () {
    string n;
    Btree* tree = new Btree();
    // tree = Btree();
    do{
        cin >> n;
        tree->insert(n);
    } while(n != "9");
    cout << '[';
    tree->PreOrder(tree->head);
    cout << ']';
    cout << endl;
    cout << '[';
    tree->InOrder(tree->head);
    cout << ']';
    cout << endl;
    cout << '[';
    tree->PostOrder(tree->head);
    cout << ']';
    cout << endl;
    return 0;
}