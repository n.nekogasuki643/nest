#include <iostream>
#include <queue>
#include <string>
#include <vector>
#include <map>
using namespace std;
struct Node{
    string val;
    Node* left;
    Node* right;
};

class Btree {
    private: 
        queue<Node*> myq;
        int* poped;
        int findMaxRange (Node* top, int deep) {
            if(top == NULL)
                return deep;
            return max(findMaxRange(top->left, deep), findMaxRange(top->right, deep)) + 1;
        }
        
    public:
        Node* head;
        Btree() {
            // head = new Node();
            poped = new int();
            *poped = 0;
        }
        void insert(Node* tmp) {
            if(myq.empty()) {
                myq.push(tmp);
                head = tmp;
            } else if( (myq.size() + *poped) % 2 == 1) {
                if(myq.front() != NULL){
                    myq.front()->left = tmp;
                    myq.push(tmp);
                } else {
                    myq.pop();
                    (*poped)++;
                    if(!myq.empty())    
                        insert(tmp);
                }
            } else {
                myq.front()->right = tmp;
                myq.push(tmp);
                myq.pop();
                (*poped)++;
            }
        }
        
        void PreOrder(Node* tmp){
            cout << tmp->val << " ";
            if(tmp->left != NULL)    
                PreOrder(tmp->left);
            if(tmp->right != NULL)
                PreOrder(tmp->right);
        }
        void InOrder(Node* tmp){
            if(tmp->left != NULL)    
                InOrder(tmp->left);
            cout << tmp->val << " ";
            if(tmp->right != NULL)
                InOrder(tmp->right);
        }
        void PostOrder(Node* tmp){
            if(tmp->left != NULL)    
                PostOrder(tmp->left);
            if(tmp->right != NULL)
                PostOrder(tmp->right);
            cout << tmp->val << " ";
        }
        int maxRangeOfBfs () {
            return findMaxRange(head, 0);
        }
        void print() {
            vector<string> myv;
            queue<Node*> myq;
            myv.push_back(head->val);
            myq.push(head);
            while(!myq.empty()) {
                if(myq.front()->left != NULL) {
                    myq.push(myq.front()->left);
                    myv.push_back(myq.front()->left->val);
                } else 
                    myv.push_back("null");
                if(myq.front()->right != NULL) {
                    myq.push(myq.front()->right);
                    myv.push_back(myq.front()->right->val);
                } else
                    myv.push_back("null");
                myq.pop();
            }
            cout << "contains: ";
            for(int i = 0; i < myv.size(); i++) {
                cout << myv[i] << " ";
            }
            cout << endl;
        }
        void check() {
            map<string, int> trees;
            trees["BfullTree"] = 0;
            trees["BcomleteTree"] = 0;
            trees["BperfectTree"] = 0;
            vector<string> myv;
            queue<Node*> myq;
            myv.push_back(head->val);
            myq.push(head);
            while(!myq.empty()) {
                if(myq.front()->left != NULL) {
                    myq.push(myq.front()->left);
                    myv.push_back(myq.front()->left->val);
                    // if(trees["BcompleteTree"] != 2)    
                        // trees["BcompleteTree"] = 3;
                } else {
                    // if(trees["BcompleteTree"] != 2)    
                        // trees["BcompleteTree"] = 1;
                    myv.push_back("null");
                }
                if(myq.front()->right != NULL) {
                    // if(trees["BcompleteTree"] == 1)
                        // trees["BcompleteTree"]++;
                    myq.push(myq.front()->right);
                    myv.push_back(myq.front()->right->val);
                } else {
                    // if(trees["BcompleteTree"] == 3)
                        // trees["BcomleteTree"]--;
                    myv.push_back("null");
                }
                myq.pop();
            }
        }
};
int main() {
    int numberOfNodes = 0;
    Btree* root;
    root = new Btree();
    string x;
    
    do{
        Node* temp = new Node();
        cin >> x;
        temp->val = x;
        if(x != "null") {
            temp->left = NULL;
            temp->right = NULL;
            root->insert(temp);
            numberOfNodes++;
        } else {
            root->insert(NULL);
        }
    } while(x != "-1");
    root->print();
    cout << "Total:" << numberOfNodes << " Nodes here && " <<"deep is "; 
    cout << root->maxRangeOfBfs(); 
    cout << ".\n";
    cout << "Pre:";
    root->PreOrder(root->head);
    cout << endl;
    cout << "In:";
    root->InOrder(root->head);
    cout << endl;
    cout << "Post:";
    root->PostOrder(root->head);
    cout << endl;
    return 0;
}