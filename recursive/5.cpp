#include <iostream>

#define mod 1000007
using namespace std;

int rec (int a, int b) {
    if(b == 0) 
        return 1;
    int half = rec(a, b / 2);
    half = (half * half) % mod;
    if(b % 2 == 1) 
        return (half * a) % mod;
    return half;
}

int main() {
    int a, b;
    cin >> a >> b;

    cout << rec(a, b) << "\n";


    return 0;
}