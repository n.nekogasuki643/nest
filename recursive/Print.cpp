#include <iostream>
#include <queue>

using namespace std;

void rec(queue<int>& a) {
    if(a.empty())
        return;
    cout << a.front() << "\n";
    a.pop();

    rec(a);
}

int main() {
    int n, b;
    cin >> n;
    queue<int> a;
    
    for(int i = 0; i < n; i++) {
        cin >> b;
        a.push(b);
    }
    
    rec(a);

    return 0;
}