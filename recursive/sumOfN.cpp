#include <iostream>

using namespace std;

int rec (int i) {
    if(i == 0)
        return 0;
    return i + rec(i - 1);
}

int main() {
    int n;
    cin >> n;
    
    cout << rec(n) << "\n";

    return 0;
}