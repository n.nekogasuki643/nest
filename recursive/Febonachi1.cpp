#include <iostream>

using namespace std;

int Feb(int n) {
    if(n < 2) 
        return 1;
    return Feb(n - 1) + Feb(n - 2);
}

int main() {
    int n;
    cin >> n;

    cout << Feb(n) << "\n";

    return 0;
}