#include <iostream>
#include <vector>

using namespace std;

void Febonachi(int n, vector<int>& memo) {
    if(n == memo.size()) {
        cout << memo[memo.size() - 1] << "\n";
        return;
    }
    if(n == 1 || n == 0)
        memo[n] = 1;
    else 
        memo[n] = memo[n - 1] + memo[n - 2];

    Febonachi(n + 1, memo);
}

int main() {
    int n;
    cin >> n;
    vector<int> tmp(n + 1);

    Febonachi(0, tmp);

    return 0;
}