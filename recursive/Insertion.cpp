#include <iostream>
#include <vector>

using namespace std;

vector<int> rec(vector<int> &num, int ind) {
    if(ind == num.size())
        return num;
    if(ind != 0) {
        if(num[ind] < num[ind - 1]) {
            swap(num[ind], num[ind - 1]);
            rec(num, ind - 1);
        }
    }
    rec(num, ind + 1);
}

int main() {
    int n;
    cin >> n;
    vector<int> num(n);

    for(int i = 0; i < n; i++)
        cin >> num[i];
    num = rec(num, 0);

    for(int i = 0; i < n; i++) 
        cout << num[i] << "\n";
    return 0;
}