#include <iostream>
#include <vector>

using namespace std;

int main () {
    int n, dig;
    cin >> n >> dig;
    vector<int> digit(10, 0);
    for(int i = 0; i <= n; i++) {
        int j = i;
        while(j > 0) {
            digit[j % 10]++;
            j /= 10;
        }
    }
    cout << digit[dig] << " \n";
    return 0;
}