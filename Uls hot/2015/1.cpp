#include <iostream>
#include <vector>

using namespace std;

bool is_dan(int n) {
    vector<bool> check(10, 0);

    while(n != 0) {
        if(check[n % 10] == 0 && n % 10 != 0) {
            check[n % 10] = 1;
            n /= 10;
        } else 
            return false; 
    }
    return true;
}

int calc(int n) {
    n++;
    while(!is_dan(n) && n < 1000000000)
        n++;
    if(n == 1000000000)
        return 0;
    return n;
}

int main () {
    int t, N;
    cin >> t;
    vector<int> results;
    for(int i = 0; i < t; i++) {
        cin >> N;
        results.push_back(calc(N));
    }
    for(int i = 0; i < results.size(); i++) {
        cout << results[i] << "\n";
    }

    return 0;
}