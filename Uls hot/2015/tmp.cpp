#define lel long long
class Solution {
public:
    vector <vector <lel> > memo;
    int r, l;
    void solve(int steps) {
        if (steps == l) return;
        for (int i = 0; i < r; i++) {
            if (i - 1 >= 0) 
                memo[steps + 1][i - 1] = (memo[steps + 1][i - 1] + memo[steps][i]) % 1000000007;
            if (i + 1 < r)
                memo[steps + 1][i + 1] = (memo[steps][i] + memo[steps + 1][i + 1]) % 1000000007;
            memo[steps + 1][i] = (memo[steps + 1][i] + memo[steps][i]) % 1000000007; 
        }
        return solve(steps + 1);
    }
    int numWays(int steps, int arrLen) {
        if (arrLen > 501) 
            arrLen = 500;
        arrlen = min(arrlen, 501);
        r = arrLen;
        l = steps;
        memo.resize(steps + 1, vector <lel>(arrLen, 0));
        memo[0][0] = 1;
        solve(0);
        return (memo[m - 1][0] + memo[m - 1][1]) % 1000000007;
    }
};