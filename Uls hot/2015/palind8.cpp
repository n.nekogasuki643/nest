#include <iostream>
#include <vector>
#include <cmath>
#define int long long
using namespace std;

vector<int> build (int range, int cur, vector<int> prev) {
    vector<int> next;
    //stop
    if(range == 1) {
        vector<int> result;
        for(int i = 1; i < 8; i++) {
            result.push_back(i);
        }
        return result;
    }
    if(cur == 0)
        next = build(range - 1, 0, prev);
    if (cur == (range + 1) / 2) {
        vector<int> result;
        int temp = 0;
        for(int i = 0; i < cur - 1; i++) {
            temp += pow(8, range - i - 1) * prev[i];
            temp += pow(8, i) * prev[i];
        }
        if(range % 2 == 0) {
            temp += pow(8, cur - 1) * prev[cur - 1];
            temp += pow(8, cur) * prev[cur - 1];
        } else
            temp += pow(8, cur - 1) * prev[cur - 1];
        result.push_back(temp);
        prev.clear();
        return result;
    }

    for(int i = 0; i < 8; i++) {
        if(cur == 0 && i == 0) 
            continue;
        prev.push_back(i);
        vector<int> temp = build(range, cur + 1, prev);
        // harvest
        for(auto el : temp) {
            next.push_back(el);
        }
        prev.pop_back();
    }
    return next;
}

pair<int, int> bsearch(int l, int r, vector<int>& palindromes, int finding) {
    if(l + 1 >= r) {
        return make_pair(palindromes[l], palindromes[r]);
    }
    if(palindromes[(r + l) / 2] > finding && palindromes[(r + l) / 2 - 1] < finding ) {
        return make_pair(palindromes[(r + l) / 2] , palindromes[(r + l) / 2 - 1]);
    }
    if(palindromes[(r + l) / 2] > finding) {
        return bsearch(l, (l + r) / 2 - 1, palindromes, finding);
    }
    return bsearch((l + r) / 2, r, palindromes, finding);
}

signed main () {
    int t;
    cin >> t;
    vector<int> prev;
    vector<int> palindromes = build(10, 0, prev);
    vector<int> end;
    for(int i = 0; i < t; i++) {
        int n;
        cin >> n;
        pair<int, int > result = bsearch(0, palindromes.size() - 1, palindromes, n);
        if(abs(n - result.first) > abs(n - result.second))
            end.push_back(result.second);
        else 
            end.push_back(result.first);
    }
    for(auto el : end) {
        cout << el << "\n";
    }

    return 0;
}