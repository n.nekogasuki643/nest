#include <iostream>
#include <vector>
#include <string>

using namespace std;

int calc(int n) {
    n++;
    vector<bool> check (10, 0);
    int temp = n, pow = 1;
    vector<int> tdigits;
    int count = 0;
    while(temp != 0) {
        tdigits.push_back(temp % 10);
        temp /= 10;
        pow *= 10;
        count ++;
    }
    vector<int> digits;
    for(int i = tdigits.size() - 1; i >= 0; i--)
        digits.push_back(tdigits[i]);
    for(int i = 0; i < digits.size(); i++) {
        while(digits[i] == 0 || (digits[i] != 10 && check[digits[i]] == 1)) {
            digits[i]++;
            for(int j = i + 1; j < digits.size(); j++)
                digits[j] = 0;
        }
        int j = i;
        if(digits[j] == 10) {
            // int j = i;
            while(digits[j] == 10) {
                digits[j] = 0;
                j--;
                if(j == -1) {
                    digits[0] = 1;
                    digits.push_back(0);
                }else 
                    digits[j]++;
            }
            for(int i = 0; i < 11; i++)
                check[i] = 0;
            i = -1;
        } else
            check[digits[i]] = 1;
    }
    for(int i = 0; i <digits.size(); i++) {
        cout << digits[i];
    }
    cout << "\n";
    // int result = 0;
    // for(int i = 0; i < digits.size(); i++) {
    //     result *= 10;
    //     result += digits[i];
    // }
    // if(result < 1000000000)
    //     return result;
    return 0;
}

int main () {
    int t, N;
    cin >> t;

    for(int i = 0; i < t; i++) {
        cin >> N;
        cout << calc(N) << "\n";
    }
    return 0;
}