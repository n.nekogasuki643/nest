#include <iostream>
#include <vector>

using namespace std;

struct Stree{
    pair<int, int> val;
    Stree *left;
    Stree *right;
    Stree *root;
    pair<int, int> param;
    // Stree *adress;
    Stree() {
        // cout << "hallo\n";
        root = this;
        root->left = NULL;
        root->right = NULL;
    }
    void build (int l, int r, vector<int> &input, Stree* node) {
        node->param.first = l;
        node->param.second = r;
        
        if(l == r) {
            node->val.first = l;
            node->val.second = input[l];
            return;
        }
        Stree *nl = new Stree();
        Stree *nr = new Stree();
        node->left = nl;
        node->right = nr;

        build(l, (r + l) / 2, input, nl);
        build((l + r) / 2 + 1, r, input, nr);

        if(node->left->val.second > node->right->val.second)
            node->val = node->right->val;
        else 
            node->val = node->left->val;

    }
    void build(vector<int> &input) {
        build(0, input.size() - 1, input, root);
    }
    pair<int, int> rmq (int i, int j, Stree * node) {
        if(node->param.first > j || node->param.second < i)
            return make_pair(-1, -1);

        if(node->param.first < i && node->param.second > j)
            return node->val;
        
        pair<int, int> l = rmq(i, j, node->left);
        pair<int, int> r = rmq(i, j, node->right);
        
        if(l.first == -1) 
            return r;
        if(r.first == -1)
            return l;
        if(l.second < r.second) 
            return l;
        return r;
    }
};

int main () {
    int n;
    cin >> n;
    cout << "nani\n";
    vector<int> input(n);
    for(int i = 0; i < n; i++) {
        cout << "kkkk\n";
        cin >> input[i];
    }
    cout << "hallo\n";
    Stree *root = new Stree();
    cout << "bambo\n"; 
    root->build(input);
    int k;
    cin >> k;
    for(int i = 0; i < k; i++) {
        int l, r;
        cin >> l >> r;
        cout << root->rmq(l, r, root).first << " indeced " << root->rmq(l, r, root).second << "\n";
    }

    return 0;
}