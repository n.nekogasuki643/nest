#include <iostream>
#include <vector>
#include <iomanip>

using namespace std;



int main () {
    int n, q;
    cin >> n >> q;
    vector<vector<unsigned long long> > dp(n, vector<unsigned long long> (q + 1, 0));
    for(int i = 1; i <= 6; i++)
        dp[0][i] = 1;
    for(int i = 1; i < n; i++) {
        for(int j = i + 1; j <= q; j++) {
            for(int l = 1; l <= 6; l++) {
                if(j - l > 0) {
                    dp[i][j] += dp[i - 1][j - l];
                } else 
                    break;
            }
            // cout << i << " " << j << " " << dp[i][j] << "\n";
        }
    }
    int count = 0;
    unsigned long long temp = dp[n - 1][q];
    cout << dp[n - 1][q] << " \n"; 
    double result = dp[n - 1][q];
    while(temp > 0) {
        temp /= 6;
        count ++;
    }
    cout << count << "\n";
    if(count < n - 4) {
        result = 0;
        cout << fixed << setprecision(3) << result << "\n";

    } else {
        while(result >= 0.001 && n > 0) {
            result /= 6;
            n--;
        }
        cout << fixed << setprecision(3) << result << "\n";
    }

    return 0;
}