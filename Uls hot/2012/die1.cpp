#include <iostream>
#include <iomanip>
#include <vector>

using namespace std;

vector<vector<double> > dp(251, vector<double> (1501, -1) );

double solve(int i, int j) {
    if(j < 1)
        return 0;
    if(i == 1 && j < 7) {
        return 1 / 6.0;
    }
    if(i == 1)
        return 0;

    if(dp[i][j] == -1) {
        dp[i][j] = 0;
        for(int l = 1; l <= 6; l++) {
            //
            dp[i][j] += (solve(i - 1, j - l) / 6.0);
            // cout << dp[i][j] << "\n";
        }
    }
    // if(dp[i][j] < 0.001)
        // return 0.000;
    return dp[i][j];
}

int main () {
    int n, q;
    cin >> n >> q;
    cout << fixed << setprecision(3) << solve(n, q) << endl;

    return 0;
}