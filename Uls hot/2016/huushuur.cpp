#include <iostream>

using namespace std;

int main (){
    int a, b, c;
    cin >> a >> b >> c;
    if(c <= a && c != 0) {
        cout << b * 2 << "\n";
        return 0;
    }
    c *= 2;
    if(c % a == 0) {
        cout << c / a * b << "\n";
    } else 
        cout << (c / a + 1) * b << "\n";

}