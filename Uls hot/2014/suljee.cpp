#include <iostream>
#include <vector>
using namespace std;

void solve(int n, vector<int> nums) {
    // cout << k << "\n";
    if(n == 0) {
        int sum1 = nums[0] + nums[2] + nums[1],
        sum2 = nums[3] + nums[4] + nums[2],
        sum3 = nums[5] + nums[6] + nums[4],
        sum4 = nums[7] + nums[8] + nums[6],
        sum5 = nums[9] + nums[1] + nums[8];
        cout << nums[0] << " " << nums[2] << " " << nums[1] << ", " << nums[3] << " " << nums[4] << " " << nums[2] << ", " << nums[5] << " " << nums[6] << " " << nums[4] << ", " << nums[7] << " " << nums[8] << " " << nums[6] << ", " << nums[9] << " " << nums[1] << " " << nums[8] <<"\n";
        if(sum1 == sum2 == sum3 == sum4 == sum5) {
            cout << "here\n";
            return;
        }
    }

    for(int i = n; i < nums.size(); i++) {
        swap(nums[i], nums[n]);
        solve(n + 1, nums);
        swap(nums[i], nums[n]);
    }
}

int main () {
    // vector<bool> memo(11, 0);
    vector<int> num(11);
    for(int i = 1; i < 11; i++)
        num[i] = i;
    solve(0, num);

    return 0;
}