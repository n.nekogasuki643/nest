#include <iostream>
using namespace std;

void solve (int row, char seat) {
    if(row < 3) {
        if(seat == 'A' || seat == 'D')
            cout << "window\n";
        else 
            cout << "edge\n";
        return;
    } 
    if(row < 21) {
        if(seat == 'A' || seat == 'F')
            cout << "window\n";
        else 
            cout << "edge\n";
        return;
    }
    if(seat == 'A' || seat == 'K') {
        cout << "window\n";
        return;
    }
    if(seat == 'C' || seat == 'D' || seat == 'G' || seat == 'H') {
        cout << "edge\n";
        return;
    }
    cout << "middle\n";
}

int main () {
    string a;
    cin >> a;
    int row = 0;
    char seat = a[a.length() - 1];
    for(int i = 0; i < a.length() - 1; i++) {
        row *= 10;
        row += (a[i] - 48);
    }
    solve(row, seat);

    return 0;
}