/*
ID: n.nekog1
LANG: C++
TASK: milk3
*/
#include <iostream>
#include <fstream>
#include <vector>
#include <queue>
#include <unordered_map>
#include <algorithm>
using namespace std;

vector<int> results;
unordered_map<int, bool> filter;
vector<int> tmp;
vector<int> bucketSize(3);

void calc (queue<vector<int> >& buckets) {
    if(buckets.size() == 0)
        return;
    if(buckets.front()[0] == 0) 
        results.push_back(buckets.front()[2]);
    for(int i = 2; i >= 0; i--) {
        for(int j = 2; j >= 0; j--) {
            tmp = buckets.front();
            if(i != j) {
                if(tmp[i] + tmp[j] <= bucketSize[j]) {
                    tmp[j] += tmp[i];
                    tmp[i] = 0;
                    if(filter[tmp[0] * 10000 + tmp[1] * 100 + tmp[2]] == true) //can change;
                        continue;
                    buckets.push(tmp);
                    cout << i << " " << j << " --------- \n";
                    for(int i = 0; i < 3; i++)
                        cout << tmp[i] <<" ";
                    cout << "\n";
                    filter[tmp[0] * 10000 + tmp[1] * 100 + tmp[2]] = true;
                }
                if(tmp[i] + tmp[j] > bucketSize[j]) {
                    tmp[i] -= (bucketSize[j] - tmp[j]);
                    tmp[j] = bucketSize[j];
                    if(filter[tmp[0] * 10000 + tmp[1] * 100 + tmp[2]] == true)
                        continue;
                    buckets.push(tmp);
                    for(int i = 0; i < 3; i++)
                        cout << tmp[i] <<" ";
                    cout << "\n";
                    filter[tmp[0] * 10000 + tmp[1] * 100 + tmp[2]] = true;
                }
            }
        }
    }
    buckets.pop();
    calc(buckets);
}

int main() {
    ifstream in ("milk3.in");
    ofstream out ("milk3.out");
    in >> bucketSize[0] >> bucketSize[1] >> bucketSize[2];
    queue<vector<int> > myqu;
    vector<int> first(3);
    first[0] = 0;
    first[1] = 0;
    first[2] = bucketSize[2];
    myqu.push(first);
    calc(myqu);
    sort(results.begin(), results.end());
    results.pop_back();
    for(int i = 0; i < results.size() - 1; i++) 
        out << results[i] << " ";
    out << results[results.size() - 1] <<"\n";
    return 0;
}