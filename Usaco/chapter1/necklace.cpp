/*
ID: n.nekog1
LANG: C++
TASK: beads
*/
#include <iostream>
#include <fstream>
#include <string>

using namespace std;
int range(int start, int end, int n) {
    int range = 0;
    if(end < start){
        for(int i = start; i < n; i++) 
            range++;
        for(int i = 0; i < end; i++)
            range++;
    } else 
        for(int i = start; i < end; i++)
            range++;
    return range;
}
int main() {
    ifstream in ("beads.in");
    ofstream out ("beads.out");

    string necklace;
    int n;

    in >> n >> necklace;
    char lastcolor, firstcolor = '1';
    int start = 0, end = 0, mid = 0, firstSeqEnd = 0, secondSeqEnd = 0, best;
    int colors = 0;
    while(end != n) {
        if(colors == 0) {
            if(necklace[end] != 'w') {
                if(firstcolor == '1')
                    firstcolor = necklace[end];
                lastcolor = necklace[end];
                colors++;
            }
            mid ++;
        } else if ( colors == 1) {
            if(necklace[end] != lastcolor && necklace[end] != 'w') {
                mid = end;
                colors++;
                lastcolor = necklace[end];
            } else {
                mid ++;
            }
        } else {
            if(necklace[end] != lastcolor && necklace[end] != 'w') {
                if(necklace[mid - 1] == 'w')
                    while(necklace[mid - 1] == 'w')
                        mid --;
                best = max(best, range(start, end, n));
                start = mid;
                if(necklace[start] == 'w')
                    colors = 0;
                else{
                    lastcolor = necklace[start];
                    colors--;
                }
                if(firstSeqEnd == 0) 
                    firstSeqEnd = mid - 1;
                if(secondSeqEnd == 0)
                    secondSeqEnd = end - 1;
                end = start;
                mid = start;
            }
        }
        end++;
    }
    if(start == 0 && end == n){
        out << n << "\n"; 
        return 0;
    }
    int tmp = end - 1;
    if(lastcolor == firstcolor){
        while(necklace[mid - 1] == 'w')
            mid --;
        best = max(best, range(mid, secondSeqEnd + 1, n));     
        best = max(best, range(start, firstSeqEnd + 1, n));
    } else {
        end = 0;
        while(necklace[end] == 'w')
            end++;
        best = max(best, range(start, end, n));
        while(necklace[mid - 1] == 'w')
            mid --;
        best = max(best, range(mid, firstSeqEnd, n));
        while(necklace[tmp] == 'w')
            tmp--;
        best = max(best, range(tmp + 1, secondSeqEnd + 1, n));
    }
    out << best << "\n";
    return 0;
}