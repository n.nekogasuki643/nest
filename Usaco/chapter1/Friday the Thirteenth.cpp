/*
ID: n.nekog1
LANG: C++
TASK: friday
*/
#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

int main() {
    ifstream in ("friday.in");
    ofstream out ("friday.out");
    
    int n;
    in >> n;
    vector<int> days(7, 0);
    int firstday = 0;
    for(int i = 0; i < n; i++) {
        for(int j = 1; j <= 12; j++) {
            days[(firstday + 5) % 7] ++;
            if((j == 4 || j == 6) || (j == 9 || j == 11)) {
                firstday += 2;
            } else {
                if(j == 2) {
                    if((((1900 + i) % 4 == 0) && (1900 + i) % 100 != 0) || (1900 + i) % 400 == 0)
                        firstday ++;
                } else {
                    firstday += 3;
                }
            }
        }
    }
    out << days[5] << " " << days[6];
    for(int i = 0; i < 5; i++)
        out << " " <<days[i];
    out << "\n";

    return 0;
}