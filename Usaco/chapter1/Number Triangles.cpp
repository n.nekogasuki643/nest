/*
ID: n.nekog1
LANG: C++
TASK: numtri
*/
#include <iostream>
#include <fstream>
#include <vector>
#include <unordered_map>

using namespace std;
vector<vector<int> > memo;
int n;
vector<vector<int> > pyramid;

int bfs(int ind, int deep) {
    if(deep == n - 1) 
        return pyramid[deep][ind];
    if (memo[deep][ind] != -1)
        return memo[deep][ind];
    memo[deep][ind] = pyramid[deep][ind] + max(bfs(ind, deep + 1), bfs(ind + 1, deep + 1));
    return memo[deep][ind];
}

int main() {
    ifstream in ("numtri.in");
    ofstream out ("numtri.out");
    in >> n;
    pyramid.resize(n);
    memo.resize(n);
    for(int i = 1; i <= n; i++) {
        vector<int> tmp(i);
        for (int j = 0; j < i; j++) {
            in >> tmp[j];
            cout << " here \n";
            memo[i - 1].push_back(-1);
        }
        pyramid[i - 1] = tmp;
    }
    out << bfs(0, 0) << "\n";

    return 0;
}