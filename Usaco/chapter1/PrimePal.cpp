/*
ID: n.nekog1
LANG: C++
TASK: pprime
*/
#include <iostream>
#include <fstream>
#include <vector>
#include <math.h>

using namespace std;
vector<bool> prime (20000, 1);
int a, b;
vector<int> results;

void Sieve() { 
    for (int p = 2; p * p < prime.size(); p++) { 
        if (prime[p] == true) { 
            for (int i = p * p; i < prime.size(); i += p) 
                prime[i] = false; 
        } 
    }
}

int calcDigit (int z) {
    int countDigit = 0;
    while(z != 0) {
        z/=10;
        countDigit ++;
    }
    return countDigit - 1;
}   
bool checkRightPrime(int number) {
    if(number<a || number > b){
        return false;
    }
    int b = sqrt(number);
    for(int i = 2; i <= b; i++) {
        if(prime[i] == true && number % i == 0) {
            return false;
        }
    }
    return true;
}

void Paldigits(int digit, int cur, int digita){
    if(digit > sqrt(digita)) {
        if(checkRightPrime(cur))
            results.push_back(cur);
        return;
    }

    if(digita == 1) {
        for(int i = 1; i <= 9; i+=2) {
            if(checkRightPrime(i)) {
                results.push_back(i);
            }
        }
        return;
    }
    if(digit == 1) {
        for(int i = 1; i <= 9; i+=2)
            Paldigits(digit * 10, cur+(i * (digit + digita/digit)), digita);
    }
     else {
        if(digit == sqrt(digita)) {
            for(int i = 0; i <= 9; i++) {
                int temp = cur + (i * digit);
                if(checkRightPrime(temp))
                    results.push_back(temp);
            }
            return; 
        }
        for(int i = 0; i <= 9; i++)
            Paldigits(digit * 10, cur+(i * (digit + digita/digit)), digita);
    }
}

int main () {
    ifstream in ("pprime.in");
    ofstream out ("pprime.out");
    in >> a >> b;
    Sieve();
    int digita = calcDigit(a), digitb = calcDigit(b), sf = pow(10, digita);
    digita = pow(10, digita);
    digitb = pow(10, digitb);
    Paldigits(1, 0, digita);
    while(digita != digitb) {
        digita *= 10;
        Paldigits(1, 0, digita);
    }
    for(int i = 0; i < results.size(); i++) 
        out << results[i] << "\n";
    return 0;
}