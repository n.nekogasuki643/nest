/*
ID: n.nekog1
LANG: C++
TASK: milk2
*/
#include <iostream>
#include <fstream>
#include <vector>
#include <map>
using namespace std;
int main() {
    ifstream in ("milk2.in");
    ofstream out ("milk2.out");
    int n;
    in >> n;
    int p1 = 0, p2 = 0;
    int start, end;
    int beststay = 0, bestpass = 0;
    in >> p1 >> p2;
    // bestpass = p1;
    // beststay = max(beststay, end - start);

    for(int i = 1; i < n; i++) {
        in >> start >> end;
        if(start < p1 && end >= p1){
            cout << p1 << " start is " << start << "\n";
            if(end > p2) {
                p2 = end;
                bestpass = 0;
            }
            p1 = start;
        } else if(start <= p2 && start >= p1){

            // cout << "start " << start << " ";
            p2 = end;
            // cout << p1 << " ";
            // cout << p2 << "\n";
        } else{
            // cout << " ????? \n";
            if(start > p2) {
                bestpass = max((start - p2), bestpass);
            } else {
                bestpass = max((p1 - end), bestpass);
            }
            // cout << beststay << " best \n";
            p1 = start;
            p2 = end;
        }
        beststay = max(p2 - p1, beststay);
    }
    if(beststay < p2 - p1) {
        cout << beststay << "\n";
        beststay = p2 - p1;
    } 
    cout << p1 << " " << p2 << "\n";
    out << beststay << " " << bestpass << "\n";
    

    return 0;
}