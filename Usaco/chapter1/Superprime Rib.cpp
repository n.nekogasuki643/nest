/*
ID: n.nekog1
LANG: C++
TASK: sprime
*/
#include <iostream>
#include <fstream>
#include <math.h>
#include <vector>

using namespace std;


int main() {
    ifstream in ("sprime.in");
    ofstream out ("sprime.out");
    int length;
    in >> length;
    vector<int> primes, result;
    // cout << length << " max";

    int min = pow(10, length - 1), max = pow(10, length);
    vector<bool> prime(max + 1, 1);
    prime[1] = false;
    for (int p = 2; p< max; p++) { 
        if (prime[p]) { 
            if(p < max && p > min && ( (p/min) == 2 || (p/min) % 2 != 0)) {
                primes.push_back(p);
            }
            // cout << length << "\n";
            for (int i = p * p; i < max; i += p)
                prime[i] = false;
        }
    }
    int j;
    for(int i = 0; i < primes.size(); i++) {
        int tmp = primes[i];
        cout << primes[i] << "\n"; 
        for(j = 0; j < length; j++) {
            if(!prime[primes[i]])
                break;
            primes[i] /= 10;
            // cout << "nani\n";
        }
        if(primes[i] == 0) {
            result.push_back(tmp);
            // cout << tmp << " ";
        }
    }
    for(int i = 0; i < result.size(); i++) 
        out << result[i] << "\n";
    return 0;
}