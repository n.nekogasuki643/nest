/*
ID: n.nekog1
LANG: C++
TASK: sprime
*/
#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <algorithm>

using namespace std;
vector<bool> prime;
vector<int> results;
void Sieve() { 
    for (int p = 2; p * p < prime.size(); p++) { 
        if (prime[p] == true) {
            for (int i = p * p; i < prime.size(); i += p) 
                prime[i] = false; 
        }
    }
}

bool checkRightPrime(int number) {
    int b = sqrt(number);
    for(int i = 2; i <= b; i++) {
        if(prime[i] == true && number % i == 0)
            return false;
    }
    return true;
}
int length;

void build(int cur, int l) {
    if(l > length) {
        return;
    }
    if (l == 1) {
        for(int i = 1; i <= 7; i+=2) {
            if(i == 1)
                build(2, 2);
            else 
                build(i, 2);
        }
    } else {
        for(int i = 1; i < 4; i += 2) {
            if(l == length) {
                if( checkRightPrime(cur * 10 + i) )
                    results.push_back(cur * 10 + i);
                if( checkRightPrime(cur * 10 + (10 - i) ) )
                    results.push_back(cur * 10 + (10 - i) );
            }
            if (checkRightPrime( cur * 10 + i ) )
                build(cur * 10 + i, l + 1);
            if (checkRightPrime( cur * 10 + 10 - i) )
                build(cur * 10 + 10 - i, l + 1);
        }
    }
    // }

}

int main() {
    ifstream in ("sprime.in");
    ofstream out ("sprime.out");
    in >> length;
    int min = pow(10, length - 1), max = pow(10, length);
    prime.resize(sqrt(max) + 1, 1);
    Sieve();
    build(0, 1);
    sort(results.begin(), results.end());
    for(int i = 0; i < results.size(); i++) 
        out<< results[i] << "\n";

    return 0;
}