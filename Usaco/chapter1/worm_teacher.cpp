/*
ID: n.nekog1
LANG: C++
TASK: wormhole
*/
#include <iostream>
#include <fstream>
#include <vector>
#include <unordered_set>
#include <queue>


using namespace std;

vector<pair<int, int> > holes(12);
int n;

int solve(vector<int>& path){
    int ans = 0;
    if (path.size() == n){

        vector<int> left, right;
        for (int i = 0; i < n; i++){
            if (path[i])
                left.push_back(i);
            else
                right.push_back(i);            
        }
        
        if (left.size() == right.size()){
            for (int i = 0; i < n; i++)
                cout << path[i];
            cout << "\n";
            
        }
        return 0;
    }
    path.push_back(0);

    ans = solve(path);
    path[path.size() - 1] = 1;
    ans += solve(path);
    path.pop_back();

    return ans;
}
int main() {

    cin >> n;

    for (int i = 0; i < n; i++)
        cin >> holes[i].first >> holes[i].second;
    

    vector<int> path;
    cout << solve(path) << endl;

    return 0;
} 