/*
ID: n.nekog1
LANG: C++
TASK: milk
*/
#include <iostream>
#include <map>
#include <fstream>
using namespace std;

int main() {
    ifstream in ("milk.in");
    ofstream out ("milk.out");
    map<int , int> farms;
    int n, amount, price, number, totalprice = 0;
    in >> amount >> n;
    for(int i = 0; i < n; i++) {
        in >> price >> number;
        farms[price] += number;
    }
    map<int , int>::iterator it = farms.begin();
    while (it != farms.end()) {
        if(amount > it->second) {
            totalprice += (it->first * it->second);
            amount -= it->second;
        } else {
            totalprice += (it->first * amount);
            break;
        }
        it++;
    }
    out << totalprice << "\n";
    

    return 0;
}