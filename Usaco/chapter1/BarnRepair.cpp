/*
ID: n.nekog1
LANG: C++
TASK: barn1
*/
#include <iostream>
#include <unordered_map>
#include <fstream>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
    ifstream in ("barn1.in");
    ofstream out ("barn1.out");
    int m, s, c, result = 0;
    in >> m >> s >> c;
    vector<int> dif;
    vector<int> location (c);
    
    for(int i = 0; i < c; i++)
        in >> location[i];
    sort(location.begin(), location.end());
    
    for(int i = 1; i < c; i++)
        dif.push_back(location[i] - location[i - 1] - 1);
    sort(dif.begin(), dif.end());
    c -= m;
    for(int i = 0; i < c; i++) {
        result += dif[i];
    }
    out << result + c << "\n";
    return 0;
}