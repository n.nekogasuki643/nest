/*
ID: n.nekog1
LANG: C++
TASK: milk2
*/
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

using namespace std;


int main() {
    ifstream in ("milk2.in");
    ofstream out ("milk2.out");
    vector<pair<int, int> > subs;
    int n, p1, p2, beststay = 0, bestpass = 0;
    in >> n;
    for(int i = 0; i < n; i++) {
        pair<int, int> param;
        in >> p1 >> p2;
        param = make_pair(p1, p2);
        subs.push_back(param);
    }
    sort(subs.begin(), subs.end());
    p1 = subs[0].first;
    p2 = subs[0].second;
    beststay = p2 - p1;
    for(int i = 1; i < n; i++) {
        if(subs[i].first <= p2) 
            p2 = max(subs[i].second, p2);
        else{
            bestpass = max(bestpass, subs[i].first - p2);
            p1 = subs[i].first;
            p2 = subs[i].second;
        }
        beststay = max(beststay, p2 - p1);
    }
    if(n == 1)
        out << p2 - p1 << " 0\n";
    else
        out << beststay << " " << bestpass << "\n";
    return 0;
}