/*
ID: n.nekog1
LANG: C++
TASK: runround
*/
#include <iostream>
#include <vector>
#include <fstream>

using namespace std;

vector<int> digits(int n) {
    vector<int> digit, tmp;
    while(n != 0) {
        digit.push_back(n % 10);
        n /= 10;
    }
    for(int i = digit.size() - 1; i >= 0; i--) 
        tmp.push_back(digit[i]);
    return tmp;
}

bool check(vector<int> digits) {
    for(int i = 0; i < digits.size(); i++) {
        if(digits[i] == 0)
            return 0;
        for(int j = i + 1; j < digits.size(); j++) 
            if(digits[i] == digits[j])
                return 0;
    }
    int size = digits.size(), first = digits[0], remainder = first % size,  last = digits[remainder];
    vector<bool> all(size, 0);
    while(first != last) {
        if(!all[remainder])
            all[remainder] = 1;
        else 
            break;
        remainder = (remainder + last) % size;
        last = digits[remainder];
    }
    if(first != last)
        return 0;
    for(int i = 1; i < size; i++)
        if(!all[i])
            return 0;
    return 1;

}

int main () {
    ifstream in("runround.in");
    ofstream out("runround.out");
    int n;
    in >> n;
    while(!check(digits(n + 1)))
        n++;
    out << n + 1 << "\n";


    return 0;
}