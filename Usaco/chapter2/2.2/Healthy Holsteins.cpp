/*
ID: n.nekog1
LANG: C++
TASK: holstein
*/

#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

vector<int> results;
vector<int> vitamin;

void solution (vector<vector<int> >& a, vector<int> sum, int n, vector<int> index) {
    int i = 0;
    if(index.size() > results.size())
        return;
    // bug
    while (i < vitamin.size() && vitamin[i] <= sum[i])
        i++;
    if(i == vitamin.size()) {
        //processing our new result
        if(index.size() <= results.size())
            results = index;
        // if(index.size() == results.size()) {
        //     for (int j = 0; j < index.size(); j++) 
        //         if(index[j] > results[j])
        //             return;
        //     results = index;
        // }
        return;
    }
    if(n == a.size())
        return;
    vector<int> tmp (vitamin.size());
    //recursive
    for(i = n; i < a.size(); i++) {
        for(int j = 0; j < vitamin.size(); j++)
            tmp[j] = sum[j] + a[i][j];
        index.push_back(i);
        solution(a, tmp, i + 1, index);
        index.pop_back();
    }
    solution(a, sum, n + 1, index);
}



int main () {
    ifstream in ("holstein.in");
    ofstream out ("holstein.out");
    int v, g;
    in >> v;
    vitamin.resize(v);
    for (int i = 0; i < v; i ++) {
        in >> vitamin[i];
    }
    in >> g;
    results.resize(g + 1);
    vector<vector<int> > a(g, vector<int> (v));
    for (int i = 0; i < g; i++)
        for (int j = 0; j < v; j++)
            in >> a[i][j];
    vector<int> sum(v, 0);
    vector<int> index;

    solution(a, sum, 0, index);

    out << results.size() << " ";
    for(int i = 0; i < results.size() - 1; i++) 
        out << results[i] + 1 << " ";
    out << results[results.size() - 1] + 1 << "\n";


    return 0;
}