/*
ID: n.nekog1
LANG: C++
TASK: lamps
*/
#include <iostream>
#include <vector>
#include <fstream>
#include <unordered_map>
#include <algorithm>
#include <queue>
#define mp make_pair
using namespace std;

vector<int> muston, mustoff;
unordered_map<int, bool> memo;
vector<vector<int> > result;


vector<bool> button (vector<bool> lamps, int which) {
    if(which == 0) {
        for(int i = 0; i < lamps.size(); i++)
            lamps[i] = !lamps[i];
        return lamps;
    }
    if(which == 1) {
        for(int i = 0; i < lamps.size(); i+=2) 
            lamps[i] = !lamps[i]; 
        return lamps;
    }
    if(which == 2) {
        for(int i = 1; i < lamps.size(); i+=2) 
            lamps[i] = !lamps[i];
        return lamps;
    }
    if(which == 3) {
        for(int i = 0; i < lamps.size(); i+=3)
            lamps[i] = !lamps[i];
        return lamps;
   }
}
int myhash(vector<int> keys) {
    int key = 0, pow = 1000;
    for(int i = 0; i < 4; i++) {
        key += keys[i] * pow;
        pow /= 10;
    }
    return key;
}
vector<vector<int> > bfs(queue<pair<vector<int>, int> >& lamps, vector<vector<int> >& result) {
    if(lamps.empty()) {
        return result;
    }
    int curdeep = lamps.front().second;
    if(curdeep == -1)
        return result;
    //initielize
    while (lamps.front().second == curdeep) {
        if(curdeep % 2 == 0)
            result.push_back(lamps.front().first);
        for(int i = 0; i < 4; i++) {
            vector<int> tmp = lamps.front().first;
            tmp[i] = (tmp[i] + 1) % 2;
            int key = myhash(tmp);
            if(!memo[key]) {
                lamps.push(mp(tmp, curdeep - 1));
                memo[key] = 1;
            }
        }
        lamps.pop();
    }
    return bfs(lamps, result);
}

bool check (vector<bool> lamps) {
    for(int i = 0; i < muston.size(); i++)
        if(!lamps[muston[i] - 1])
            return 0;
    for(int i = 0; i < mustoff.size(); i++)
        if(lamps[mustoff[i] - 1])
            return 0;
    return 1;
}

int main () {
    ifstream in ("lamps.in");
    ofstream out ("lamps.out");
    int n, c;
    in >> n >> c;
    int k;
    do{
        in >> k;
        muston.push_back(k);
    } while(k != -1);
    muston.pop_back();
    do{     
        in >> k;
        mustoff.push_back(k);  
    } while(k != -1);
    mustoff.pop_back();
    memo[0] = 1;

    queue<pair<vector<int>, int> > myq;
    pair<vector<int>, int> first = mp(vector<int> (4, 0), c);
    myq.push(first);
    vector<vector<int> > result;
    result = bfs(myq, result);

    vector<vector<bool> > config;
    for(int i = 0; i < result.size(); i++) {
        // cout << "here\n";
        vector<bool> lamps(n, 1);
        for(int j = 0; j < 4; j++) {
            if(result[i][j] == 1) {
                lamps = button(lamps, j);
            }
        }
        if(check(lamps))
            config.push_back(lamps);
    }
    sort(config.begin(), config.end(), [](vector<bool> a, vector<bool> b) {
        for (int i = 0; i < a.size(); i++) {
            if (!a[i] && b[i]) return true;
            if (a[i] && !b[i]) return false;
        }
        return false;
    });
    if(config.size() != 0)
        for(int i = 0; i < config.size(); i++) {
            for(int j = 0; j < config[i].size(); j++) 
                out << config[i][j];
            out << endl;
        }
    else 
        out << "IMPOSSIBLE\n";

    return 0;
}