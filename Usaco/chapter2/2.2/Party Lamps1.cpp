/*
ID: n.nekog1
LANG: C++
TASK: lamps
*/
#include <iostream>
#include <vector>
#include <fstream>
#include <unordered_map>
#include <algorithm>

using namespace std;

vector<int> muston, mustoff, result;
unordered_map<int, bool> memo;


vector<bool> button (vector<bool> lamps, int which) {
    if(which == 0) {
        for(int i = 0; i < lamps.size(); i++)
            lamps[i] = !lamps[i];
        return lamps;
    }
    if(which == 1) {
        for(int i = 0; i < lamps.size(); i+=2) 
            lamps[i] = !lamps[i]; 
        return lamps;
    }
    if(which == 2) {
        for(int i = 1; i < lamps.size(); i+=2) 
            lamps[i] = !lamps[i];
        return lamps;
    }
    if(which == 3) {
        for(int i = 0; i < lamps.size(); i+=3)
            lamps[i] = !lamps[i];
        return lamps;
    }
}

void interrupt (int n, int key) {
    // cout << n <<"\n";
    cout << key << "\n";
    if(n == 0) {
        memo[key] = 1;
        cout << key << "waasdfas fkey\n";
        result.push_back(key);
        return;
    }
    memo[key] = 1;
    n--;
    int count = 0;
    if(memo.find((key + 1000) % 2000) == memo.end()) {
        cout << key << " connected\n";
        interrupt(n, (key + 1000) % 2000);
        count++;
    }
    int first = key / 1000, second = key % 1000; 
    if(memo.find(first * 1000 + (second + 100) % 200) == memo.end()) {
        cout << key << " connected\n";
        interrupt(n, first * 1000 + (second + 100) % 200);
        count++;
    }
    first = key / 100;
    second = key % 100;
    if(memo.find(first * 100 + (second + 10) % 20) == memo.end()) {
        cout << key << " connected\n";
        interrupt(n, first * 100 + (second + 10) % 20);
        count++;
    }
    first = key / 10;
    second = key % 10;
    if(memo.find(first * 10 + (second + 1) % 2) == memo.end()) {
        cout << key << " connected\n";
        interrupt(n, first * 10 + (second + 1) % 2);
        count++;
    }
    if(count == 0 && n % 2 == 0) {
        interrupt(0, key);
        cout << "dude\n";
    }

    // interrupt(n, key);
    // interrupt(n, (key + 1000) % 2000);
    // interrupt(n, first * 1000 + (second + 100) % 200);
    // interrupt(n, first * 100 + (second + 10) % 20);
    // interrupt(n, first * 10 + (second + 1) % 2);

}
bool check (vector<bool> lamps) {
    for(int i = 0; i < muston.size(); i++)
        if(!lamps[muston[i] - 1])
            return 0;
    for(int i = 0; i < mustoff.size(); i++)
        if(lamps[mustoff[i] - 1])
            return 0;
    return 1;
}

int main () {
    ifstream in ("lamps.in");
    ofstream out ("lamps.out");
    int n, c;
    in >> n >> c;
    int k;
    do{
        in >> k;
        muston.push_back(k);
    } while(k != -1); 
    muston.pop_back();
    do{
        in >> k;
        mustoff.push_back(k);  
    } while(k != -1);
    mustoff.pop_back();
    interrupt(c, 0);
    vector<int> count(4, 0);
    vector<vector<bool> > config;
    for(int i = 0; i < result.size(); i++) {
        int j = 0, pow = 1000;
        cout << "there\n";
        while(j < 4) {
            count[j] = result[i] / pow;
            result[i] = result[i] % pow;
            pow /= 10;
            j++;
        }
        vector<bool> lamps(n, 1);
        for(j = 0; j < 4; j++)
            if(count[j])
                lamps = button(lamps, j);
        if(check(lamps)) {
            bool chal = 0;
            for(int i = 0; i < config.size(); i++) {
                if(config[i] == lamps) {
                    chal = 1;
                    break;
                }
            }
            if(!chal)
                config.push_back(lamps);
        }
    }
    sort(config.begin(), config.end(), [](vector<bool> a, vector<bool> b) {
        for (int i = 0; i < a.size(); i++) {
            if (!a[i] && b[i]) return true;
            if (a[i] && !b[i]) return false;
        }
        return false;
    });
    if(config.size() != 0)
        for(int i = 0; i < config.size(); i++) {
            for(int j = 0; j < config[i].size(); j++) 
                out << config[i][j];
            out << endl;
        }
    else 
        out << "IMPOSSIBLE\n";
    return 0;
}