/*
ID: n.nekog1
LANG: C++
TASK: subset
*/

#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

vector<vector<long long> > memo(40, vector<long long> (400, -1));

long long calc(int sum, int j) {
    if(sum == 0)
        return memo[j][0] = 1;
    if(sum < 0 || j == 0)
        return 0;

    if(memo[j][sum] != -1) 
        return memo[j][sum];
    return memo[j][sum] = calc(sum - j, j - 1) + calc(sum, j - 1); 

}

int main () {
    ifstream in ("subset.in");
    ofstream out ("subset.out");
    int n, result = 0;
    in >> n;
    n = n * (n + 1) / 2; 
    if(n % 2)
        out << 0 << "\n";
    result = calc(n >> 1, n) / 2;

    return 0;
}