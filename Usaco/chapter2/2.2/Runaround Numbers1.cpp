/*
ID: n.nekog1
LANG: C++
TASK: runround
*/
#include <iostream>
#include <vector>
#include <fstream>

using namespace std;

vector<int> digits(int n) {
    vector<int> digit, tmp;
    while(n != 0) {
        digit.push_back(n % 10);
        n /= 10;
    }
    for(int i = digit.size() - 1; i >= 0; i--) 
        tmp.push_back(digit[i]);
    return tmp;
}

bool check(vector<int> digits, int remainder, vector<bool>& memo) {
    
}

int main () {
    ifstream in("runround.in");
    ofstream out("runround.out");
    int n;
    in >> n;
    vector<int> digit = digits(n + 1);
    vector<bool> memo (digit.size(), 0);
    while(!check(digits(n + 1), 0, memo)) {
        n++;
    }
    out << n + 1 << "\n";


    return 0;
}