/*
ID: n.nekog1
LANG: C++
TASK: sort3
*/
#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

int main () {
    ifstream in("sort3.in");
    ofstream out("sort3.out");
    int n, j = 0, count = 0, sum = 0;
    in >> n;
    vector<int> nums(3, 0);
    vector<int> a(n);
    vector<pair<int, int> > include(3);

    int x[] = {2, 1, 1};
    int y[] = {3, 3, 2};

    for (int i = 0; i < n; i++) {
        in >> a[i];
        nums[a[i] - 1]++;
    }
    for (int i = 0; i < 3; i++) {
        int m = 0, n = 0;
        for (int k = j; k < nums[i] + j; k++) {
            if(a[k] == x[i])
                m++;
            if(a[k] == y[i])
                n++;
            include[i] = make_pair(m , n);
        }
        j += nums[i];
    }
    for (int i = 0; i < 3; i++ ) {
        nums[i] = include[i].first + include[i].second;
        sum += nums[i];
    }
    int tmp = min(include[0].first, include[1].first);
    sum -= (tmp * 2);
    count += tmp;
    tmp = min(include[0].second, include[2].first);
    sum -= (tmp * 2);
    count += tmp;
    tmp = min(include[1].second, include[2].second);
    sum -= (tmp * 2);
    count += tmp;
    count += (sum/3 * 2);
    out << count << "\n";
     
    


    return 0;
}