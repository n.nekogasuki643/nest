/*
ID: n.nekog1
LANG: C++
TASK: ariprog
*/
#include <algorithm>
#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

const int limit = 250 * 250 * 2 + 1;

int main() {
    ifstream in("ariprog.in");
    ofstream out("ariprog.out");

    int n, m;
    in >> n >> m;
    
    vector<bool> isBisquare(limit, false);
    for (int i = 0; i <= m; i++)
        for (int j = i; j <= m; j++)
            isBisquare[i * i + j * j] = true;
    
    vector<int> bisquares;
    for (int i = 0; i < limit; i++)
        if (isBisquare[i]) 
            bisquares.push_back(i);
    
    bool isSequence;
    int ak, bisquareCount = bisquares.size();
    vector<pair<int , int> > results;

    int maxD = bisquares[bisquareCount - 1] / (n - 1) + 1;
    for (int d = 1; d < maxD; d++) {
        for (int i = 0; i < bisquareCount; i++) {
            isSequence = true;
            for (int k = 1; k < n; k++) {
                ak = bisquares[i] + k * d;
                if (ak >= limit || !isBisquare[ak]) {
                    isSequence = false;
                    break;
                }
            }

            if (isSequence) results.push_back(make_pair(d, bisquares[i]));
        }
    }

    for (auto p : results)
        out << p.second << ' ' << p.first << endl;
    if (results.size() == 0)
        out << "NONE" << endl;
    return 0;
}