/*
ID: n.nekog1
LANG: C++
TASK: frac1
*/
#include <iostream>
#include <fstream>

using namespace std;

int n;
ifstream in ("frac1.in");
ofstream out ("frac1.out");

void print (int a, int b, int a1, int b1) {
    if(b + b1 > n)
        return;
    print(a, b, a1 + a, b1 + b);
    out << a1 + a << "/" << b + b1 << "\n";
    print(a + a1, b + b1, a1, b1);
}

int main () {
    in >> n;
    out << "0/1\n";
    print(0, 1, 1, 1);                                         
    out << "1/1\n";

    return 0;
}