/*
ID: n.nekog1
LANG: C++
TASK: castle
*/
#include <iostream>
#include <fstream>
#include <math.h>
#include <vector>
#include <algorithm>

using namespace std;

vector<vector<int> > a;
vector<vector<bool> > graph;
int most = 0, rooms = 0, cur = 0;
int moveY[] = {1, 0, -1, 0}; //int moveY[] = {0, -1, 0, 1};
int moveX[] = {0, 1, 0, -1}; // int moveX[] = {-1, 0, 1, 0};
void connection (int i, int j) {
    cur++;
    int walldir = 8;
    graph[i][j] = 1;
    for (int idx = 0; idx < 4; idx++) {
        int y = i + moveY[idx], x = j + moveX[idx];
        // cout << y << " y " << x << " x\n";
        if (x < 0 || y < 0 || x >= graph[0].size() || y >= graph.size()) {
            if(a[i][j] >= walldir)
                a[i][j] -= walldir;
            walldir /= 2;
            // cout << walldir << " " << a[i][j] << " wall and\n";
            continue;
        }

        if (a[i][j] < walldir && graph[y][x] == 0) {
            // cout << "cont : " << i << ' ' << j << " too " << y << ' ' << x << '\n';
            connection(y, x);
        }
        if(a[i][j] >= walldir)
            a[i][j] -= walldir;
        walldir /= 2;
    }
    // cout << cur << "size here \n";
}

int main () {
    ifstream in ("castle.in");
    ofstream out ("castle.out");
    int m, n;
    in >> m >> n;
    a.resize(n);
    graph.resize(n);
    for(int i = 0; i < n; i++) {
        a[i].resize(m);
        graph[i].resize(m);
        for(int j = 0; j < m; j++) {
            in >> a[i][j];
            graph[i][j] = 0;
        }
    }
    for(int i = 0; i < n; i++) {
        for(int j = 0; j < m; j++) {
            if(!graph[i][j]) {
                rooms++;
                cur = 0;
                // cout << i << " " << j << "\n";
                connection(i, j);
                most = max(most, cur);
            }
        }
    }
    cout << rooms << " \n";
    cout << most << " \n";



    return 0;
}