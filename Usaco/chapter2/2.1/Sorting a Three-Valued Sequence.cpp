/*
ID: n.nekog1
LANG: C++
TASK: sort3
*/
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

using namespace std;

int main () {
    ifstream in("sort3.in");
    ofstream out("sort3.out");
    int godswap = 0, badswap = 0, n, result = 0;
    in >> n;
    vector<int> a(n);
    vector<int> b(n);

    for(int i = 0; i < n; i++) {
        in >> a[i];
        b[i] = a[i];
    }
    sort(b.begin(), b.end());

    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++) {
            if(a[i] != a[j] && b[i] != b[j] && a[i] == b[j] && a[j] == b[i]) {
                godswap ++;
                a[i] = b[i];
                a[j] = b[j];
            }
        }
    result += godswap;

    for (int i = 0; i < n; i++)
        if (a[i] != b[i])
            badswap++;
    result += badswap/3 * 2;
    out << result << "\n";

    return 0;
}