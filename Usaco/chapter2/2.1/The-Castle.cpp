/*
ID: n.nekog1
LANG: C++
TASK: castle
*/
#include <iostream>
#include <vector>
#include <fstream>
#define mp make_pair

using namespace std;

vector<vector<int> > castle;
vector<int> sizes;

int count_size(int i, int j, vector<vector<int> >& graph, int color) {
    graph[i][j] = color;
    int count = 1;
    if( !(castle[i][j] & 8) && graph[i + 1][j] == 0) 
        count += count_size(i + 1, j, graph, color);
    if( !(castle[i][j] & 4) && graph[i][j + 1] == 0) 
        count += count_size(i, j + 1, graph, color);
    if( !(castle[i][j] & 2) && graph[i - 1][j] == 0) 
        count += count_size(i - 1, j, graph, color);
    if( !(castle[i][j] & 1) && graph[i][j - 1] == 0) 
        count += count_size(i, j - 1, graph, color);
    return count;
}

pair< pair <int, char>, pair<int, int> > calc_largest (vector<vector<int> >& graph) {
    int m = graph.size(), n = graph[0].size();
    int largest = -1, sum;
    char dir;
    pair<int, int> loc;
    for(int i = 0; i < n; i++) {
        for(int j = m - 1; j >= 0; j--) {
            if( j != 0 && ( (castle[j][i] & 2) != 0) && (graph[j][i] != graph[j - 1][i]) ) {
                // color is starting from 1;
                sum = sizes[graph[j][i] - 1] + sizes[graph[j - 1][i] - 1];
                if(largest < sum) {
                    dir = 'N';
                    largest = sum;
                    loc = mp(j, i);
                }
            }
            if( i != n - 1 &&( (castle[j][i] & 4) != 0) && (graph[j][i] != graph[j][i + 1]) ) {
                sum = sizes[graph[j][i] - 1] + sizes[graph[j][i + 1] - 1];
                if(largest < sum) {
                    dir = 'E';
                    largest = sum;
                    loc = mp(j, i);
                }
            }
        }
    }
    pair<int, char> wall = mp(largest, dir);
    return mp(wall, loc);
}


int main () {
    ifstream in ("castle.in");
    ofstream out ("castle.out");
    int n, m, room = 0, most = 0;
    in >> n >> m;
    castle.resize(m);
    for(int i = 0; i < m; i++) {
        castle[i].resize(n);
        for(int j = 0; j < n; j++) {
            in >> castle[i][j];
        }
    }
    vector<vector<int> > graph (m, vector<int> (n, 0));
    for(int i = 0; i < m; i++) {
        for(int j = 0; j < n; j++) {
            if(!graph[i][j]) {
                room++;
                sizes.push_back(count_size(i, j, graph, room));
                most = max(sizes.back(), most);
            }
        }
    }
    out << room<< "\n" << most << "\n";
    pair<pair<int, char>, pair<int, int> >tmp = calc_largest(graph);
    out << tmp.first.first << "\n" << tmp.second.first + 1 << " " << tmp.second.second + 1 << " " << tmp.first.second<< "\n";

    return 0;
}