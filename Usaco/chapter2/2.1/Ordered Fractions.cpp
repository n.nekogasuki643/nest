/*
ID: n.nekog1
LANG: C++
TASK: frac1
*/
#include <iostream>
#include <fstream>
#include <vector>
#define mp make_pair

using namespace std;

vector<pair<int, int> > ins_sort (vector<pair<int, int> >& a, pair<int, int> frac) {
    if (a.size() == 0 || a.back().first * frac.second < a.back().second * frac.first) {
        a.push_back(frac);
        return a;
    }
    if (a.back().first * frac.second == a.back().second * frac.first) 
        return a;
    pair<int, int> last = a.back();
    a.pop_back();
    a = ins_sort(a, frac);
    a.push_back(last);
    return a;
}
void make_frac(int n, vector<pair<int, int> >& fractions) {
    fractions.push_back(mp(0, 1));
    
    for(int i = n; i >= 1; i--)
        fractions.push_back(mp(1, i));    
    
    for(int j = 2; j <= n; j++)
        for(int i = n; i >= j; i--)
            fractions = ins_sort(fractions, mp(j, i));

}

int main () {
    ifstream in ("frac1.in");
    ofstream out ("frac1.out");
    int n;
    in >> n;
    vector<pair<int, int> > fracs;
    make_frac(n, fracs);
    for (int i = 0; i < fracs.size(); i++)
        out << fracs[i].first << "/" << fracs[i].second << "\n";

    return 0;
}