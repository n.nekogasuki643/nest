/*
ID: n.nekog1
LANG: C++
TASK: prefix
*/
#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <unordered_map>
// #define mp make_pair
using namespace std;

unordered_map<string, bool> memo;

int dfs (string str, vector<string>& prim, string& target) {
    int best = str.size(), size = prim.size();
    memo[str] = 1;
    for (int i = 0; i < size; i++) {
        if (memo[str + prim[i]]) continue;
        if (str.length() + prim[i].length() <= target.length() && target.find(str + prim[i]) == 0) {
            best = max(dfs(str + prim[i], prim, target), best);
        }
    }
    return best;
}

int main () {
    ifstream in("prefix.in");
    ofstream out ("prefix.out"); 
    vector<string> primitives;
    string str, tmp = "";
    in >> str;

    while (str != ".") {
        primitives.push_back(str);
        in >> str;
    }
    str = "";
    getline(in, tmp);
    while (!in.eof()) {
        getline(in, tmp);
        // cout << tmp << endl;
        str += tmp;
    }
    // cout << str << endl;
    // tmp = "";
    out << dfs(tmp, primitives, str)<< "\n";

    return 0;
}