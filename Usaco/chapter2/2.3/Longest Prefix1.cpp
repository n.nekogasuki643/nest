/*
ID: n.nekog1
LANG: C++
TASK: prefix
*/

#include <iostream>
#include <fstream>
#include <vector>
#include <unordered_map>

using namespace std;

string str = "";
unordered_map<int, bool> memo;

bool check(int j, string &now) {
    if (now.size() > str.size() - j)
        return 0; 
    for(int i = 0; i < now.size(); i++) 
        if (now[i] != str[j + i])
            return 0;
    return 1;
}
//worth
int solve (int j, vector<string>& prim) {
    int best = 0;
    memo[str.size() - j] = 1;
    for(int i = 0; i < prim.size(); i++) {
        if (check(j, prim[i]) && memo.find(str.size() - prim[i].size() - j) == memo.end()) {
            best = max(int(prim[i].size() + solve(j + prim[i].size(), prim) ), best);
        }
    }
    return best;
}

int main () {
    ifstream in("prefix.in");
    ofstream out ("prefix.out"); 
    vector<string> primitives;
    string tmp = "";
    in >> str;
    while (str != ".") {
        primitives.push_back(str);
        in >> str;
    }
    str = "";
    
    while (in >> tmp) {
        str += tmp;
    }

    out << solve(0, primitives) << "\n";

    return 0;
}