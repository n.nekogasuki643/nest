/*
ID: n.nekog1
LANG: C++
TASK: prefix
*/

#include <iostream>
#include <fstream>
#include <vector>
#include <unordered_map>
#include <queue>

using namespace std;

string str;

bool check(int j, string &now) {
    if (now.size() > str.size() - j)
        return 0; 
    for(int i = 0; i < now.size(); i++) 
        if (now[i] != str[j + i])
            return 0;
    return 1;
}

int main () {
    ifstream in ("prefix.in");
    ofstream out ("prefix.out");
    vector<string> primitives;
    string tmp = "";
    in >> str;
    while (str != ".") {
        primitives.push_back(str);
        in >> str;
    }
    str = "";
    
    while (in >> tmp) {
        str += tmp;
    }
    vector<bool> dp (str.length() + 1, 0);
    queue<int> bfs;
    for(int i = 0; i < primitives.size(); i++)
        if(check(0, primitives[i])) {
            int temp = primitives[i].size();
            bfs.push(temp);
            dp[temp] = 1;
        }
    while(!bfs.empty()) {
        for(int i = 0; i < primitives.size(); i++) {
            int temp = bfs.front() + primitives[i].length();
            if(check(bfs.front(), primitives[i]) && !dp[temp]){
                bfs.push(temp);
                dp[temp] = 1;
            }
        }
        bfs.pop();
    }
    int max = 0;
    for(int i = 0; i < dp.size(); i++)
        if(dp[i]) max = i;
    out << max << "\n";
    return 0;
}