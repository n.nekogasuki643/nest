/*
ID: n.nekog1
LANG: C++
TASK: nocows
*/

#include <iostream>
#include <fstream>
#include <vector>
#include <unordered_map>
#define pb push_back
#define mp make_pair
#define MOD 9901
#define ull unsigned long long
using namespace std;

ull Choose (int a, int b) {
    if(a == 0)
        return 1;
    if(a / 2 > b) 
        b = a - b;
    ull ans = 1, div = 1;
    for(int i = 1; i <= b; i++) {
        // cout<<"here\n";
        ans = ans * (a - i + 1);
        div *= i;
        if(ans % div == 0) {
            ans = ans / div;
            div = 1;
        }
    }
    // cout << (ans / div) << " ///////////////\n";
    return (ans / div) % MOD;
}

int main () {
    ifstream in("nocows.in");
    ofstream out("nocows.out");
    int n, k;
    in >> n >> k;
    // vector<vector<int> > dp(k, vector<int> (n, 0));
    vector<int> prev(n, 0), now(n, 0);
    vector<pair<int, int> > temp, level, emp;
    prev[0] = 1;
    temp.pb(mp(1, 0));                              //level 1
    for(int i = 1; i < k; i++) {
        // cout << i <<" i\n";
        for(int j = 0; j < temp.size(); j++) {
            // cout << j << " j\n";
            // cout << temp[j].first << " "; 
            for(int l = 0; l < temp[j].first; l++) {
                // cout << l << " l\n";
                int t = (l + 1) * 2 + temp[j].second;
                if(t >= n)
                    break;
                level.pb(mp((l + 1) * 2, t));
                cout << (prev[temp[j].second] * Choose(temp[j].first, l + 1) ) << " urjwer\n"; 
                now[t] = (now[t] + (prev[temp[j].second] * Choose(temp[j].first, l + 1) ) ) % MOD;

            }
        }
        cout << i << " level\n";
        for(auto el : level) {
            cout << "(" << el.first << ", " << el.second << ")" << " ";
        }
        cout << "\n";

        for(int j = 0; j < n; j++) 
            cout << prev[j] << " ";
        cout << "\n";
        prev = now;
        for(int j = 0; j < n; j++)
            now[j] = 0;
        temp = level;
        level.clear();
    }
    for(int i = 0; i < n; i++) 
        cout << prev[i] << " ";
    cout << "\n";
    // for(int i = 0; i < k; i++) {
    //     for(int j = 0; j < n; j++) {
    //         cout << dp[i][j] << " ";
    //     }
    //     cout << "\n";
    // }
    out << prev[n - 1] << "\n";
    return 0;
}