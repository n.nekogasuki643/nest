#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

vector<long long> dividers(long long n) {
    int temp = sqrt(n);
    vector<bool> sieve(temp + 1, 1);
    vector<long long> result;
    vector<int> prime;
    for (int i = 2; i <= temp; i++) {
        if (sieve[i]) {
            for (int j = i * i; j <= temp; j += i)
                sieve[j] = false;
        }
    }
    for (int p = 2; p <= temp; p++) {
        if (sieve[p]) {
            prime.push_back(p);
        }
    }
    int l = 0;
    while(l < prime.size() && n > prime[l]) {
        if(n % prime[l] == 0) {
            n /= prime[l];
            result.push_back(prime[l]);
        } else {
            l++;
        }
    }
    if(n != 1)
        result.push_back(n);
    return result;
}

void color(vector<vector<char> > &last, int begin, int a, int b) {
    for(int i = begin; i < begin + a; i++) {
        for(int j = begin + a; j < begin + a + b; j++) {
            last[i][j] = 'Y';
            last[j][i] = 'Y';
        }
    }
    return;
}

int main () {
    long long sum = 0, n;
    // long long n;
    cin >> n;
    if(n == 1) {
        cout << 2 << "\n" << "NY\n" << "YN\n";
        return 0;
    }
    vector<long long> div = dividers(n);
    for(int i = 0; i < div.size(); i++)
        sum += div[i];
    vector<vector<char> > last (sum + 2, vector<char> (sum + 2, 'N'));
    int first = 2;
    for(int i = 2; i < 2 + div[0]; i++) {
        last[0][i] = 'Y';
        last[i][0] = 'Y';
    }
    for(int i = 0; i < div.size() - 1; i++) {
        color(last, first, div[i], div[i + 1]);
        first += div[i];
    }
    for(int i = first; i < first + div[div.size() - 1]; i++) {
        last[1][i] = 'Y';
        last[i][1] = 'Y';
    }
    cout << sum + 2 << "\n";
    for(int i = 0; i < sum + 2; i++) {
        for(int j = 0; j < sum + 2; j++) {
            cout << last[i][j];
        }
        cout << endl;
    }

    return 0;
}