#include <iostream>
#include <algorithm>
#include <vector>
#include <math.h>

using namespace std;

vector<int> primes(int n) {
    vector<bool> sieve(n, 1);
    vector<long long> result;
    vector<int> prime;
    for (int i = 2; i * i <= n; i++) {
        if (sieve[i]) {
            for (int j = i * i; j <= n; j += i)
                sieve[j] = false;
        }
    }
    for (int p = 2; p <= n; p++) {
        if (sieve[p]) {
            cout << p << " ";
            prime.push_back(p);
        }
    }
    cout << "\n";
    return prime;
}

int main () {
    int g, n;
    cin >> g;
    vector<int> primenumbers = primes(10);
    vector<int>::iterator low;
    for (int i = 0; i < g; i++) {
        cin >> n;
        if(n == 1) {
            cout << "Bob\n";
        } else {
            low = upper_bound(primenumbers.begin(), primenumbers.end(), n);
            int count = (low - primenumbers.begin());
            cout << count << "\n";
            if(count % 2 != 0)
                cout << "Alice\n";
            else 
                cout << "Bob\n";
        }
    }
    return 0;
}