#include <iostream>
#include <vector>

using namespace std;

int median (vector<int> arr, int l, int r) {
    sort(arr.begin() + l, arr.begin() + r);
    return arr[(l + r) / 2];
}

int partition (vector<int>& arr, int l, int r, int x) {
    int i;
    for (i = l; i <= r; i++)
        if(arr[i] == x) 
            break;
    swap(arr[r], arr[i]);
    i = l;
    for (int j = l; j < r; j++) {
        if(arr[j] <= x) {
            swap(arr[j], arr[i]);
            i++;
        }
    }
    swap(arr[i], arr[r]);
    return i;
}

int kthlowest(vector<int>& arr, int l, int r, int k) {
    vector<int> medians;
    int i;
    for(i = l; i + 5 <= r; i += 5)
        medians.push_back(median(arr, i, i + 5));

    if(i <= r)
        medians.push_back(median(arr, i, r));
    int medofmed = median(medians, 0, medians.size());
    int index = partition(arr, l, r, medofmed);
    if(index == k)
        return arr[index];
    if(index > k)
        return kthlowest(arr, l, index - 1, k);
    return kthlowest(arr, index + 1, r, k);
}

int main () {
    int n, k;
    cin >> n;
    vector<int> arr(n);

    for(int i = 0; i < n; i++)
        cin >> arr[i];
    cin >> k;
    k--;
    cout << kthlowest(arr, 0, n - 1, k) << " is result\n" ;
    return 0;
}