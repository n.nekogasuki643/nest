#include <iostream>
#include <string>
#include <vector>

using namespace std;

int main () {
    string a, b;
    cin >> a >> b;
    bool found = false;
    for(int i = 0; i < a.length(); i++) {
        int j, temp = i;
        for(j = 0; j < b.length(); j++) {
            if(temp != i && a[i + j] == b[0])
                temp = i + j;
            if(a[j + i] != b[j])
                break;
        }
        if(j == b.length()) {
            cout << i << " ";
            found = true;
        }
        i = temp;
    }
    if(!found) 
        cout << "no solution here\n";

    return 0;
}