#include <iostream>
#include <stack>
#include <queue>

using namespace std;

struct pile {
    struct Node {
        int val;
        Node *next;
    };
    Node *front;
    Node *top;
    int lenght;
    pile() {    
        front = NULL;
        top = NULL;
        lenght = 0;
    }
    void insert(int tmp) {
        lenght++;
        if (front == NULL) {
            Node *temp = new Node();
            temp->val = tmp;

            front = temp;
            top = front;
        } else {
            Node *temp = new Node();
            temp->val = tmp;
            top->next = temp;
            top = temp;
        }
    }
    void pop_front () {
        lenght--;
        if(lenght == 0)
            return;
        front = front->next;
    }
    void pop_back () {
        lenght--;
        Node *temp = front;
        if(lenght == 0)
            return;
        for (int i = 1; i < lenght; i++)
            temp = temp->next;
        top = temp;
    }
};

int Ciel (vector<pile *> &piles) {
    int best = 0, index;
    for (int i = 0; i < piles.size(); i++) {
        if(piles[i]->lenght > 0 && piles[i]->front->val > best) {
            best = piles[i]->front->val;
            index = i;
        }
    }
    piles[index]->pop_front();
    return best;
}

int Jiro (vector<pile *> &piles) {
    int best = 0, index;
    for (int i = 0; i < piles.size(); i++) {
        if(piles[i]->lenght > 0 && piles[i]->top->val > best) {
            best = piles[i]->top->val;
            index = i;
        }
    }
    piles[index]->pop_back();
    return best;
}



int main () {
    int n, s, sum = 0, sum1 = 0, sum2 = 0;
    cin >> n;
    vector<pile *> piles;
    vector<pair<int,int> > oddindeces;
    for (int i = 0; i < n; i++) {
        cin >> s;
        sum += s;
        pile *Tmp = new pile();
        for (int j = 0; j < s; j++) {
            int k;
            cin >> k;
            if(s % 2 == 1 && j == (s / 2 + 1))
                oddindeces.push_back(make_pair(k, i));
            Tmp->insert(k);
        }
        piles.push_back(Tmp);
    }

    
    for (int i = 0; i < sum; i++) {
        if (i % 2 == 0)
            sum1 += Ciel(piles);
        else  
            sum2 += Jiro(piles);
    }
    cout << sum1 << " " << sum2 << "\n";

    return 0;
}