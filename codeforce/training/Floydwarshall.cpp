#include <iostream>
#include <vector> 
#define INF 100000

using namespace std;

int main () {
    int n, k, m;
    cin >> n >> k;
    // cin >> m;
    vector<vector<int> > path(n, vector<int> (n, INF));
    // vector<int> must(m);
    // for (int i = 0; i < m; i++)
        // cin >> must[i];
    for (int i = 0; i < n; i++)
        path[i][i] = 0;

    for (int i = 0; i < k; i++) {
        int from, to, cost;
        cin >> from >> to >> cost;
        path[from - 1][to - 1] = cost;
    }
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            for (int k = 0; k < n; k++) {
                path[i][j] = min(path[i][j], path[i][k] + path[k][j]);
            }
        }
    }
    // for(int i = 0; i < n; i++) {
    //     for(int j = 0; j < n; j++) {
    //         if(path[i][j] != INF) 
    //             cout << path[i][j] << " ";
    //         else cout << "- ";
    //     }
    //     cout << "\n";
    // }
    return 0;
}