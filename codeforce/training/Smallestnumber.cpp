#include <iostream>
#include <vector>
#include <math.h>
#define ll long long
using namespace std;

vector<char> symbol(3);

long long solve (int s, vector<int> num) {
    if(s == 2) {
        int product;
        for(int i = 0; i < 3; i++) {
            if(symbol[i] == '+') {
                if(i == 0) product = num[0] + num[1];
                else product = product + num[i + 1];
            } else {
                if(i == 0) product = num[0] * num[1];
                else product = product * num[i + 1];
            }
        }
        // cout << product << "\n";
        return product;
    }
    ll result = 1000000000000;
    for(int i = s; i < 4; i++) {
        swap(num[i], num[s]);
        result = min(solve(s + 1, num), result);
        swap(num[i], num[s]);
    }
    return result;
}
int main () {
    vector<int> numbers(4);
    for(int i = 0; i < 4; i++)
        cin >> numbers[i];
    for(int i = 0; i < 3; i++)
        cin >> symbol[i];
    cout << solve(0, numbers) << "\n";
    return 0;
}