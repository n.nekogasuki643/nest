#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

bool high(int a, int b) {
    return a > b;
}

int main () {
    int n, s, sum1 = 0, sum2 = 0;
    cin >> n;
    vector<int> odds;
    for (int i = 0; i < n; i++) {
        cin >> s;
        vector<int> pile;
        for(int j = 0; j < s; j++) {
            int k;
            cin >> k;
            pile.push_back(k);
        }
        for(int j = 0; j < s / 2; j++) {
            sum1+=pile[j];
            sum2+=pile[s - j - 1];
        }
        if (s % 2 == 1) 
            odds.push_back(pile[s / 2]);
    }
    sort(odds.begin(), odds.end(), high);
    for(int i = 0; i < odds.size(); i++) {
        if(i % 2 == 0)
            sum1 += odds[i];
        else
            sum2 += odds[i];
    }
    cout << sum1 << " " << sum2 << "\n";

    return 0;
}