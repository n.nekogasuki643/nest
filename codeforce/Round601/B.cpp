#include <iostream>
using namespace std;

int main () {
    int T;
    cin >> T;
    int n, m, d, result;
    for(int i = 0; i < T; i++) {
        cin >> n >> m;
        d = abs(n - m);
        result = 0;
        result += d / 5;
        d %= 5;
        result += d / 2;
        d %= 2;
        result += d;
        cout << result << "\n";
    }
	return 0;
}