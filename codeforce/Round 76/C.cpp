#include <iostream>
#include <vector>
#include <unordered_map>
#include <algorithm>

#define mod 998244353
using namespace std;

int main () {
    int n;
    cin >> n;
    unordered_map<int, int> primes;
    for(int i = 0; i < n; i++) {
        int k;
        cin >> k;
        primes[k]++;
    }
    vector<int> powers;
    for(unordered_map<int, int>::iterator it = primes.begin(); it != primes.end(); ++it) {
        powers.push_back(it->second + 1);
    }
    if(powers.size() == 1) {
        cout << 1 << endl;
        return 0;
    }
    sort(powers.begin(), powers.end());
    int l;
    for(l = 0; l < powers.size(); l++) {
        if(powers[l] != 2)
            break;
    }
    if(l == powers.size()) {
        cout << powers.size() << "\n";
        return 0;
    }
    long long ans = powers[powers.size() - 2] % mod;

    for (int i = powers.size() - 3; i >= 0; i--)
        ans = (ans * powers[i] ) % mod;

    cout << ans << endl;

    return 0;
}