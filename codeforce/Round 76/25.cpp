#include <iostream>

using namespace std;
bool check(string a) {
    for (int i = 0; i < a.size() - 1; i++ )
        if (a[i] == a[i + 1]) return false;
    // cout << a << "\n";
    return true;
}
int main () {
    int ans = 0;
    for(int i = 100000; i < 1000000; i+= 25) {
        string tmp = to_string(i);
        if(check(tmp)) ans++;
    }
    cout << ans << '\n';
    return 0;
}