#include <iostream>
#include <vector>
using namespace std;

int main () {
    int n, k;
    cin >> n;
    vector<pair<int, int> > p(n);
    pair<int, int> knack;
    for(int i = 0; i < n; i++) 
        cin >> p[i].first >> p[i].second;
    cin >> k;
    vector<int> dp1(k + 1, 0);
    vector<int> dp2(k + 1);
    for(int i = 0; i < n; i++) {
        for(int j = 0; j <= k; j++)
            if(j - p[i].second >= 0) dp2[j] = max(dp1[j - p[i].second] + p[i].first, dp1[j]);
            else dp2[j] = dp1[j];
        dp1 = dp2;
    }
    cout << dp2[k] << "\n";

    return 0;
}