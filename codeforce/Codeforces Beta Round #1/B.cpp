#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

bool check(string a) {
    bool ind = false;
    for(int i = 0; i < a.length(); i++) {
        if(a[i] >= '0' && a[i] <= '9') 
            ind = true;
        else if(ind == true) {
            // ind = false;
            return 0;
        }
    }
    return 1;
}

void sovle(string a) {
    int r = 0, c = 0, s = 0;
    if(check(a)) {
        int i = 0;
        for(; (a[i] <= '9' && a[i] >= '0') != 1; i++) {
            c = c * 26 + (int(a[i] - 'A') + 1);
        }
        for(; i < a.length(); i++) {
            r = r * 10 + int(a[i] - '0');
        }
        cout << 'R' << r << "C" << c << '\n';
    } else {
        int i = 1;
        for(; a[i] != 'C'; i++) {
            r = r * 10 + int(a[i] - '0');
        }
        i++;
        for(; i < a.length(); i++) {
            c = c * 10 + int(a[i] - '0');
        }
        string column = "";
        while (c > 26) {
            column += char( ( (c / 26) % 26) + int('A') - 1);
            c = c - (c / 26 * 26);
            if(c <= 26) break;
        }
        column += char(c + int('A') - 1);
        cout << column << r << "\n";
    }
    return ;
}

int main () {
    int n;
    cin >> n;
    vector<string> result;
    for(int i = 0; i < n; i++) {
        string a;
        cin >> a;
        // result.push_back(sovle(a)); 
        sovle(a);
    }

    return 0;
}