#include <iostream>
#include <string>
#include <vector>

using namespace std;
 
 
 
int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
 
    int k;
    cin >> k;
 
    while (k--)
    {
        int n;
        cin >> n;
 
        string s, t;
        cin >> s >> t;
        int letters[26] = {};
        for (int i = 0; i < n; ++i)
        {
            ++letters[s[i] - 'a'];
            ++letters[t[i] - 'a'];
        }
 
        bool p = false;
        for (int count: letters)
        {
            if (count % 2 != 0)
            {
                p = true;
                break;
            }
        }
        if (p)
        {
            cout << "NO\n";
            continue;
        }
 //confirmed
        vector<pair<int, int> > ans;
        for (int i = 0; i < n; ++i)
        {
            if (s[i] != t[i])
            {
                int j;
                for (j = i + 1; j < n; ++j)
                    if (s[i] == s[j])
                        break;
                if (j != n)
                {
                    ans.push_back({j, i});
                    swap(s[j], t[i]);
                    continue;
                }
 
                for (j = i + 1; j < n; ++j)
                    if (s[i] == t[j])
                        break;
 
                ans.push_back(make_pair(j, j));
                swap(s[j], t[j]);
                ans.push_back(make_pair(j, i));
                swap(s[j], t[i]);
            }
        }
        cout << "YES\n";
        cout << ans.size() << '\n';
        for (auto p: ans)
            cout << p.first + 1 << ' ' << p.second + 1 << '\n';
    }
 
    return 0;
}