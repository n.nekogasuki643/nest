#include <iostream> 
 
using namespace std;
 
long long gcd(long long a, long long b) {
    if(a  == 0) return b;
    return gcd(b % a, a);
}
 
void solve() {
    long long r, b, k;
    cin >> r >> b >> k;
    if(r > b) {
        swap(r, b);
    }
    if(b > r * k) {
        cout << "REBEL\n";
        return;
    }
    int G = gcd(r, b);
    int x = 0;
    x = (b - G) / r + 1;
    if((b - G) % r == 0 ) {
        x--;
    }
    if(k <= x) {
        cout << "REBEL\n";
        return;
    }
    cout << "OBEY\n";
}
 
int main () {
    int t;
    cin >> t;
 
    for(int i = 0; i < t; i++) {
        solve();
    }
 
    return 0;
}