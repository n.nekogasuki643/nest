#include <iostream>
#include <vector>

using namespace std;

int main () {
    string a;
    cin >> a;
    char now = a[0];
    int p1 = 0, p2 = a.length()-1, t1 = p1, t2 = p2, sum;
    if(a.length() == 1) {
        cout << 0 << "\n";
        return 0;
    }
    while(p1 < p2){
        while(a[p1] == now)
            p1++;
        while(a[p2] == now)
            p2--;
        // cout << p1 << " " << p2 << "\n";
        if(p1 == t1 || p2 == t2) {
            cout << 0 << "\n";
            break;
        }
        if(p1 >= p2){
            // cout << "here\n";
            if(p1 - p2 < 3) cout << 0 << '\n';
            else cout << p1 - p2 << '\n';
            break;
        }
        sum = p1 - t1 + t2 - p2;
        now = a[p1];
        if(sum < 3) {
            cout << 0 << '\n';
            break;
        }
        t1 = p1;
        t2 = p2;
    }
    return 0;
}