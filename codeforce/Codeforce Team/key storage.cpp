#include <iostream>
#include <vector>
#include <algorithm>
#define ll long long
using namespace std;

vector<int> tovector(ll n){
    vector<int> ans;
    int now = 2;
    while(n != 0){
        ans.push_back(n % now);
        n /= now;
        now++;
    }
    return ans;
}

ll factorial(ll n){
    if(n == 0)
        return 1;
    return n * factorial(n - 1);
}

ll solve(ll n) {
    vector<int> k = tovector(n), cnt(21, 0), gg(21, 0);
    sort(k.begin(), k.end());
    // for(auto el : k) 
    //     cout << el << " ";
    // cout << "\n----------\n";
    ll now, ans, div = 1, endwithzr = 1;
    if(k[0] == 0){
        now = k.size() - k.back();
        // cout << now <<" zoer\n";
        endwithzr *= now;
        for(int i = k.size() - 2; i > 0; i--) {
            if(k[i + 1] != 1 && k[i + 1] != 0 && !(k[i + 1] == 2 && k[i] == 0))
                now += k[i + 1] - k[i] - 1;
            if(k[i + 1] == 1 || k[i + 1] == 0)
                now--;
            // cout << now << " zeor\n";
            endwithzr *= now;
        }

        for (int i = 1; i < k.size(); i++)
            gg[k[i]]++;
        
        for(int i = 0; i < 21; i++)
            div *= factorial(gg[i]);

        endwithzr /= div;
    }else{
        endwithzr = 0;
    }
    div = 1;
    now = k.size() + 1 - k.back();
    ans = now;
    // cout << now << '\n';
    for(int i = k.size() - 2; i >= 0; i--) {
        if(k[i + 1] != 1 && k[i + 1] != 0 && !(k[i + 1] == 2 && k[i] == 0))
                now += k[i + 1] - k[i] - 1;
            if(k[i + 1] == 1 || k[i + 1] == 0)
                now--;

        
        // cout << now << '\n';
        ans *= now;
    }
    for (int i = 0; i < k.size(); i++)
        cnt[k[i]]++;
    
    for(int i = 0; i < 21; i++)
        div *= factorial(cnt[i]);
    return ans / div - endwithzr;
}

int main () {
    ll t, n;
    cin >> t;
    vector<int> result(t);
    for(int i = 0; i < t; i++) {
        cin >> n;
        result[i] = solve(n);
    }

    for(int i = 0; i <t; i++){
        cout << result[i] - 1<< '\n';
    }
    return 0;
}