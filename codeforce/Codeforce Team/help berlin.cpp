#include <bits/stdc++.h>
using namespace std;
int num;
void divide(int st, int end, vector<int>& numbers) {
    if (end < st) return;
    int mid = (st + end) / 2;
    numbers[mid] = num;
    if (st == end) return;
    num++;
    divide(st, mid - 1, numbers);
    divide(mid + 1, end, numbers);
}
int main() {
    int t, n, noneed;
    cin >> t;
    while(t--){
        cin >> n;
        num = 1;
        for(int i = 0; i < n; i++)
            cin >> noneed;
        vector <int> numbers(n);
        divide(0, n - 1, numbers);
        for (int i = 0; i < n; i++) 
            cout << numbers[i] << ' ';
        cout << '\n';
    }
}