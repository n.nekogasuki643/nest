#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
void solve() {
    int s = 0;
    vector <int> num(3);
    for (int i = 0; i < 3; i++) {
        cin >> num[i];
        s += num[i];
    }
    sort(num.begin(), num.end());
    
    if (num.back() >= num[0] + num[1]) {
        cout << (num[0] + num[1]) << '\n';
        return;
    }
    if (s % 2 == 1) { 
        cout << (s - 1) / 2 << '\n';
        return;
    }
    cout << s / 2 << '\n';
}
signed main () {
    int t;
    cin >> t;
    while(t > 0) {
        solve();
        t--;
    }
        
	return 0;
}