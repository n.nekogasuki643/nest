#include <iostream>
#include <set>
#include <math.h>
using namespace std;
void solve() {
    long long numbs, lim;
    cin >> numbs;
    lim = sqrt(numbs);

    set <long long> answer;
    for (int i = 0; i <= lim; i++) {
        answer.insert(i);
        if (i != 0) {
            answer.insert(numbs / i);
        }
    }
    
    cout << answer.size() << '\n';
    for (auto q: answer) cout << q << ' ';
    cout << '\n';
}
signed main () {
    int t;
    cin >> t;
    while(t > 0) {
        solve();
        t--;   
    }
	return 0;
}