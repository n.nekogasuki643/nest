#include <iostream>
#include <string>
#include <vector>

using namespace std;

string solve (int n, long long k, string &binary) {
    vector<int> index;
    for (int i = 0 ; i < n; i++)
        if (binary[i] == '0')
            index.push_back(i);
    for (int i = 0; i < index.size(); i++) {
        if (k >= index[i] - i) {
            swap(binary[index[i]], binary[i]);
            k -= (index[i] - i);
        } else {
            swap(binary[index[i]], binary[index[i] - k]);
            k = 0;
        }
    }
    return binary;
}

int main () {
    int q, n;
    long long k;
    cin >> q;
    string binary;
    vector<string> result;
    for (int i = 0; i < q; i++) {
        cin >> n >> k >> binary;
        result.push_back(solve(n, k, binary));
    }
    for (int i = 0; i < q; i++)
        cout << result[i] << "\n";


    return 0;
}