#include <iostream>
#include <vector>
#include <set>
#define first first
#define second second

using namespace std;

int main() {
	int n;
	cin >> n;

	set<pair<int, int> > num;
	for(int i = 0; i < n; i++) {
        int temp;
		cin >> temp;
		num.emplace(temp, n - i);
	}

	int a;
	cin >> a;
	vector<pair<int, int> > q(a);
	for(int i = 0; i < a; i++) {
        cin >> q[i].first;
        cin >> q[i].second;
    }

	for(int i = 0; i < a; i++) {
        // que(num, n, q[i].first, q[i].second);
        set<pair<int, int> > sub;

        set<pair<int, int> >::iterator it = num.end();
        it--;
        for(int j = 0; j < q[i].first; j++) {
            auto number = *it;
            if (it != num.begin()) {
                it--;
            }
            sub.emplace(n - number.second, number.first);
        }
        cout << next(sub.begin(), q[i].second - 1)->second << endl;
    }
	return 0;
}
