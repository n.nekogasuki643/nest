#include <bits/stdc++.h>
#define pb push_back
#define mp make_pair

using namespace std;
bool cus(pair <long long, long long>&x, pair <long long, long long>&y) {
    return x.second < y.second;
}
int main () {
    long long t;
    cin >> t;
    for(int el; el < t; el++) {
        long long n, m, l;
        cin >> n;
        vector <pair <long long, long long> > arr;
        for (long long i = 0; i < n; i++) {
            cin >> m >> l;
            arr.pb(mp(m, l));
        }
        if (n == 1) {
            cout << 0 << '\n';
            continue;
        }

        sort(arr.begin(), arr.end());
        m = arr.back().first;
        sort(arr.begin(), arr.end(), cus);

        l = arr[0].second;
        if (m - l < 0)
            cout << 0 << "\n";
        else 
            cout << m - l << '\n';
    }
	return 0;
}