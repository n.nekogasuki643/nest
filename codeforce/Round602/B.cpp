#include <iostream>
#include <vector>
using namespace std;

#define ll long long
#define pb push_back
#define mp make_pair

void solve () {
    int p;
    cin >> p;
    vector<int> nums(p);
    vector<int> ans(p);
    int now = 0;
    for(int i = 0; i < p; i++) {
        cin >> nums[i];
        if(nums[i] < now) 
            now = -1;
        now = nums[i];
    }
    if(now == -1) {
        cout << -1 << "\n";
        return;
    }
    vector<bool> check(p + 1, 0);
    int use = 1;
    ans[0] = nums[0];
    check[ans[0]] = 1;
    if(use == ans[0])
        use ++;
    for(int i = 1; i < p; i++) {
        if(nums[i] > nums[i - 1]) {
            ans[i] = nums[i];
            check[ans[i]] = 1;
        } else {
            // int j;
            while(check[use] && use <= nums[i]) {
                use ++;
            }
            if(use > nums[i]) {
                cout << -1 << "\n";
                return;
            }
            ans[i] = use;
            check[use] = 1;
            use++;
            // if(j == nums[i]) {
            //     cout << -1 << "\n";
            //     return ;
            // }
        }
    }
    for(int i = 0; i < p; i++) {
        cout << ans[i] << " ";
    }
    cout << "\n";
}

int main () {
    int t;
    cin >> t;

    while (t > 0) {
        solve();
        t--;
    }

    return 0;
}