#include <iostream>
#include <vector>
#define pb push_back
#define mk make_pair
using namespace std;
 
int main () {
    int a, b, c, d;
    cin >> a >> b >> c >> d;
    if(d == c + 1 && a == 0 && b == 0) {
        cout << "YES\n";
        cout << 3 << " ";
        for(int i = 0; i < c; i++) {
            cout << 2 << " " << 3 << " ";
        }
        cout << "\n";
        return 0;
    }
    if(a == b + 1 && c == 0 && d == 0) {
        cout << "YES\n";
        cout << 0 << " ";
        for(int i = 0; i < b; i++) {
            cout << 1 << " " << 0 << " ";
        }
        cout << "\n";
        return 0;
    }
    if(a == 0 && b == 0 && d == 0 && c == 1) {
        cout << "YES\n";
        cout << 2 << "\n";
        return 0;
    }
    if(a == 0 && d == 0 && c == 0 && b == 1) {
        cout << "YES\n";
        cout << 1 << "\n";
        return 0;
    }
 
    if(d > c || a > b) {
        cout << "NO\n";
        return 0;
    }
    c = c - d;
    b = b - a;
    if(abs(c - b) > 1) {
        cout << "NO\n";
        return 0;
    }
    vector<int> start, end, mid;
    if(c == b) {
        for(int i = 0; i < d; i++) {
            start.pb(3);
            start.pb(2);
        }
        for(int i = 0; i < a; i++) {
            end.pb(1);
            end.pb(0);
        }
        for(int i = 0; i < c; i++) {
            mid.pb(1);
            mid.pb(2);
        }
    } else {
        if(c > b) {
            start.pb(2);
            c--;
            for(int i = 0; i < d; i++) {
                start.pb(3);
                start.pb(2);
            }
            for(int i = 0; i < a; i++) {
                end.pb(1);
                end.pb(0);
            }
            for(int i = 0; i < c; i++) {
                mid.pb(1);
                mid.pb(2);
            }
        } else {
            for(int i = 0; i < d; i++) {
                start.pb(3);
                start.pb(2);
            }
            for(int i = 0; i < a; i++) {
                end.pb(1);
                end.pb(0);
            }
            end.pb(1);
            b--;
            for(int i = 0; i < c; i++) {
                mid.pb(1);
                mid.pb(2);
            }
        }
    }
    vector<int> result;
    for(int i = 0; i < start.size(); i++) {
        result.pb(start[i]);
    }
    for(int i = 0; i < mid.size(); i++) {
        result.pb(mid[i]);
    }
    for(int i = 0; i < end.size(); i++) {
        result.pb(end[i]);
    }
    cout << "YES\n";
    for(int i = 0; i < result.size(); i++) {
        cout << result[i] << " ";
    }
    cout << "\n";
 
    return 0;
}