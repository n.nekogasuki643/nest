#include <algorithm>
#include <iostream>
#include <vector>
#define ss second
#define PII pair<int, int>
using namespace std;

vector<PII> comp(const vector<int>& a) {
  vector<PII> results;

  int curnum = a[0], cnt = 0;
  for(int i = 0; i < a.size(); i++) {
    if (a[i] != curnum) {
      results.push_back(make_pair(curnum, cnt));
      curnum = a[i];
      cnt = 0;
    }
    cnt++;
  }
  results.push_back(make_pair(curnum, cnt));

  return results;
}


void solve(vector<PII> a, int n) {
    if (n < 6) {
        cout << "0 0 0" << endl;
        return;
    }
    vector<int> cnt(a.size());
    cnt[0] = a[0].ss;
    for (int i = 1; i < a.size(); i++)
        cnt[i] = cnt[i - 1] + a[i].ss;

    int mid = n / 2, index;
    auto midcnt = lower_bound(cnt.begin(), cnt.end(), mid);
    if (*midcnt > mid) 
        midcnt--;
    index = midcnt - cnt.begin();
    if (index < 0) {
        cout << "0 0 0" << endl;
        return;
    }
    int g = cnt[0];
    auto silver = lower_bound(cnt.begin(), cnt.end(), g + (g + 1));
    int s = *silver - g;
    int b = cnt[index] - *silver;
    if (b <= g) {
        cout << "0 0 0" << endl;
        return;
    }
    cout << g << ' ' << s << ' ' << b << endl;
}

int main() {
  int t;
  cin >> t;

  for(int i = 0; i < t; i++) {
    int n;
    cin >> n;

    vector<int> a(n);
    for(int i = 0; i < n; i++)
        cin >> a[i];

    if (n < 6) {
        cout << "0 0 0" << endl;
        continue;
    }
    solve(comp(a), n);
  }

  return 0;
}
