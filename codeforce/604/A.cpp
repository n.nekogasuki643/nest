#include <bits/stdc++.h>
using namespace std;

int main () {
    int t;
    cin >> t;
    for(int i = 0; i < t; i++) {
        string str;
        cin >> str;
        int s = str.size();
        char digit[] = {'a', 'b', 'c'};
        string fin = "abc?";
        bool brl = false;
        for (int i = 0; i < s - 1; i++) {
            for (int j = 0; j < 3; j++) {
                if (str[i] == digit[j] && str[i + 1] == digit[j]) {
                    cout << -1 << '\n';
                    brl = true;
                    break;
                }
            }
            if(brl)
                break;
        }
        if(brl)
            continue;
        string result = "";
        for (int i = 0; i < s; i++) {
            if (str[i] != '?') {
                result += str[i];
                continue;
            }
            vector <bool> position(4, true);
            if (0 <= i - 1)
                position[fin.find(str[i - 1])] = false;
            if (0 <= i - 1) 
                position[fin.find(result.back())] = false;
            if (s > i + 1) 
                position[fin.find(str[i + 1])] = false;
            for (int j = 0; j < 4; j++) {
                if (position[j]) {
                    result += digit[j];
                    break;
                }
            }
        }
        cout << result << '\n';
    }
    return 0;
}
 
 