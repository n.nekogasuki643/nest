#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;
#define forn(a, b) for (int a = 0; a < b; a++)

vector<pair<int, int> > compress(const vector<int>& a) {
  vector<pair<int, int> > results;

  int currentNumber = a[0], count = 0;
  forn(i, a.size()) {
    if (a[i] != currentNumber) {
      results.push_back(make_pair(currentNumber, count));
      currentNumber = a[i];
      count = 0;
    }
    count++;
  }
  results.push_back(make_pair(currentNumber, count));

  return results;
}


void solve(vector<pair<int, int> > a, int n) {
  vector<int> count(a.size());
  count[0] = a[0].second;
  for (int i = 1; i < a.size(); i++)
    count[i] = count[i - 1] + a[i].second;

  int mid = n / 2, maxIdx;
  auto midCount = lower_bound(count.begin(), count.end(), mid);
  if (*midCount > mid) midCount--;
  maxIdx = midCount - count.begin();
  if (maxIdx < 0) {
    cout << "0 0 0" << endl;
    return;
  }
  int g = count[0];
  auto silverStart = lower_bound(count.begin(), count.end(), g + (g + 1));
  int s = *silverStart - g;
  int b = count[maxIdx] - *silverStart;
  if (b <= g) {
    cout << "0 0 0" << endl;
    return;
  }
  
  cout << g << ' ' << s << ' ' << b << endl;
}

int main() {
  int T;
  cin >> T;

  while (T > 0) {
    int n;
    cin >> n;

    vector<int> a(n);
    forn(i, n) cin >> a[i];

    if (n < 6) {
      cout << "0 0 0" << endl;
      goto next;
    }
    solve(compress(a), n);

    next:
    T--;
  }

  return 0;
}
